<footer>
	<div class="container">
		<p>Copyright Hilton Hotels 2020. <a href="#">Privacy Policy</a></p>
	</div>
</footer>

<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="jquery-ui/jquery-ui.js"></script>
<script>
$( function() {
	$( "#datepicker" ).datepicker();
	$( "#datepicker2" ).datepicker();
});

$(window).scroll(function(){
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
});
</script>

<script src="src/js/lightslider.js"></script> 
<script>
$(document).ready(function() {
    $('#image-gallery').lightSlider({
        gallery:true,
        item:1,
        thumbItem:9,
        slideMargin: 0,
        speed:500,
        auto:true,
        loop:true,
        onSliderLoad: function() {
            $('#image-gallery').removeClass('cS-hidden');
        }  
    });
});
</script>

</body>
</html>