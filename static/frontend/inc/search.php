<section id="search_bar_top">
	<div class="container">
		<form class="form-inline" role="form">
			<div class="form-group">
				<input type="text" name="checkin" class="form-control" placeholder="Check-in" id="datepicker">
			</div>
			<div class="form-group">
				<input type="text" name="checkout" class="form-control" placeholder="Check-out" id="datepicker2">
			</div>
			<div class="form-group">				
				<select name="guest" class="form-control">
					<option value="">---Guest--</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>				
			</div>
			<div class="form-group">
				<select name="kids" class="form-control">
					<option value="">---Kids--</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>				
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-book">Check Avaiability</button>
			</div>
		</form>

	</div>
</section>