<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotel Booking</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/hotel_frontend.css">
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" media="screen and (max-width: 1200px)" href="css/hotel_responsive.css">

<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="fontawesome-free-5.15.1-web/css/all.min.css">
<link rel="stylesheet"  href="src/css/lightslider.css"/>


    
</head>
<body>