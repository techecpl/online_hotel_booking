
<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<header>
  	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    	<ol class="carousel-indicators">
      		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    	</ol>
    	<div class="carousel-inner" role="listbox">
      		<div class="carousel-item active" style="background-image: url('banners/b1.jpg')"></div>
      		<div class="carousel-item" style="background-image: url('banners/b2.jpg')"></div>
      		<div class="carousel-item" style="background-image: url('banners/b3.jpg')"></div>
    	</div>
    	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
          	<span class="sr-only">Previous</span>
        </a>
    	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          	<span class="carousel-control-next-icon" aria-hidden="true"></span>
          	<span class="sr-only">Next</span>
        </a>
  	</div>
</header>

<section id="checkin_form">
	<div class="inner">
		<h3>Book Rooms</h3>
		<div class="form-group">
			<input type="text" name="checkin" class="form-control" placeholder="Check-in" id="datepicker">
		</div>
		<div class="form-group">
			<input type="text" name="checkout" class="form-control" placeholder="Check-out" id="datepicker2">
		</div>
		<div class="form-group">
			<select name="guest" class="form-control">
				<option value="">---Guest--</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
			</select>
		</div>
		<div class="form-group">
			<select name="kids" class="form-control">
				<option value="">---Kids--</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
			</select>
		</div>
		<div class="form-group text-center">
			<button type="submit" class="btn btn-book">Check Avaiability</button>
		</div>
	</div>
</section>


<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img src="imgs/about.png" class="img-fluid">
			</div>
			<div class="col-md-7">
				<div class="about_box">
					<h1>Welcome to Hilton Hotels</h1>
					<p class="sm-head">High quality accommodation services</p>
					<p style="text-justify">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

					<p style="text-justify">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="hotel_list_hm">
	<div class="container">
		<h2>Our Hotels</h2>
		<p class="sm-head">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy</p>

		<div id="hotelCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
			<!-- Carousel indicators -->
			<ol class="carousel-indicators">
				<li data-target="#hotelCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#hotelCarousel" data-slide-to="1"></li>
			</ol>   
			<!-- Wrapper for carousel items -->
			<div class="carousel-inner">
				<div class="carousel-item active">
					<div class="row">						
						<div class="col-sm-4">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="imgs/hotel1.jpg" class="img-fluid" alt="">
								</div>
								<div class="thumb-content">
									<h4>Hotel Name Here</h4>
									<p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
									<div class="star-rating">
										<ul class="list-inline">
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										</ul>
									</div>
									<a href="#" class="btn btn-primary">View Detail</a>
								</div>						
							</div>
						</div>						
						<div class="col-sm-4">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="imgs/hotel1.jpg" class="img-fluid" alt="">
								</div>
								<div class="thumb-content">
									<h4>Hotel Name Here</h4>
									<p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
									<div class="star-rating">
										<ul class="list-inline">
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										</ul>
									</div>
									<a href="#" class="btn btn-primary">View Detail</a>
								</div>						
							</div>
						</div>						
						<div class="col-sm-4">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="imgs/hotel1.jpg" class="img-fluid" alt="">
								</div>
								<div class="thumb-content">
									<h4>Hotel Name Here</h4>
									<p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
									<div class="star-rating">
										<ul class="list-inline">
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										</ul>
									</div>
									<a href="#" class="btn btn-primary">View Detail</a>
								</div>						
							</div>
						</div>
					</div>
				</div>
				<div class="carousel-item">
					<div class="row">						
						<div class="col-sm-4">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="imgs/hotel1.jpg" class="img-fluid" alt="">
								</div>
								<div class="thumb-content">
									<h4>Hotel Name Here</h4>
									<p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
									<div class="star-rating">
										<ul class="list-inline">
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										</ul>
									</div>
									<a href="#" class="btn btn-primary">View Detail</a>
								</div>						
							</div>
						</div>						
						<div class="col-sm-4">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="imgs/hotel1.jpg" class="img-fluid" alt="">
								</div>
								<div class="thumb-content">
									<h4>Hotel Name Here</h4>
									<p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
									<div class="star-rating">
										<ul class="list-inline">
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										</ul>
									</div>
									<a href="#" class="btn btn-primary">View Detail</a>
								</div>						
							</div>
						</div>						
						<div class="col-sm-4">
							<div class="thumb-wrapper">
								<div class="img-box">
									<img src="imgs/hotel1.jpg" class="img-fluid" alt="">
								</div>
								<div class="thumb-content">
									<h4>Hotel Name Here</h4>
									<p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
									<div class="star-rating">
										<ul class="list-inline">
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
											<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										</ul>
									</div>
									<a href="#" class="btn btn-primary">View Detail</a>
								</div>						
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Carousel controls -->
			<a class="carousel-control-prev" href="#hotelCarousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="carousel-control-next" href="#hotelCarousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</div>
</section>


<section id="facility_hm">
	<div class="opacity"></div>
	<div class="container">
		<div class="inner_box">
			<h2>Our Amenities</h2>
			<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
			<ul class="list">
				<li><i class="fas fa-swimming-pool"></i> Swimming pool</li>
				<li><i class="fas fa-dumbbell"></i> Gym & yoga</li>
				<li><i class="fas fa-ship"></i> Boat tours</li>
				<li><i class="fas fa-spa"></i> Spa & massage</li>
				<li><i class="fas fa-swimmer"></i> Surfing lessons</li>
				<li><i class="fas fa-microphone"></i> Conference room</li>
				<li><i class="fas fa-umbrella-beach"></i> Private Beach</li>
				<li><i class="fas fa-water"></i> Diving & snorkling</li>
			</ul>
		</div>
	</div>
</section>


<section id="rooms_hm">
	<div class="container">
		<h2>Rooms & Suites</h2>
		<p class="sm-head">Check out now our best rooms</p>

		<div class="row">
			<div class="col-md-6">
				<div class="room">
					<div class="top lg-box">
						<div class="img-hover-zoom"><img src="imgs/room-full-1.jpg" class="img-fluid"></div>
						<ul class="large">
							<li><i class="fas fa-wifi"></i></li>
							<li><i class="fas fa-female"></i></li>
							<li><i class="fas fa-bath"></i></li>
							<li><i class="fas fa-utensils"></i></li>
							<li><i class="fas fa-coffee"></i></li>
							<li><i class="fas fa-glass-martini"></i></li>
							<li><i class="fas fa-mobile-alt"></i></li>
							<li><i class="fas fa-tv"></i></li>
							<li><i class="fas fa-snowflake"></i></li>
						</ul>
					</div>
					<div class="content">
						<h3>Deluxe Room - <span>INR 3500/-</span></h3>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<div class="room">
							<div class="top">
								<ul>
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
								</ul>
								<div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
							</div>
							<div class="content">
								<h3>Honeymoon Room - <span>INR 3500/-</span></h3>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="room">
							<div class="top">
								<ul>
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
								</ul>
								<div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
							</div>
							<div class="content">
								<h3>Single Room - <span>INR 3500/-</span></h3>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="room">
							<div class="top">
								<ul>
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
								</ul>
								<div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
							</div>
							<div class="content">
								<h3>Family Room - <span>INR 3500/-</span></h3>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="room">
							<div class="top">
								<ul>
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
								</ul>
								<div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
							</div>
							<div class="content">
								<h3>King Room - <span>INR 3500/-</span></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="restaurant_hm">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="img_box">
					<img src="imgs/restaurant.jpg" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6">
				<div class="content">
					<h3>Our Restaurant</h3>
					<p>A Stockholm restaurant featuring international classics with unexpected contemporary twists. Every detail prepared, cooked and cared for with craftsmanship and devotion. A high focus on seasonal and local ingredients.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 order">
				<div class="content">
					<h3>Spa & Fitness</h3>
					<p>A Stockholm restaurant featuring international classics with unexpected contemporary twists. Every detail prepared, cooked and cared for with craftsmanship and devotion. A high focus on seasonal and local ingredients.</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="img_box">
					<img src="imgs/gym.jpg" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</section>


<?php include('inc/footer.php'); ?>
