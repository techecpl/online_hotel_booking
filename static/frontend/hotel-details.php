

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
	<h1>Hotel Name Here</h1>
</section>


<?php include('inc/search.php'); ?>


<section id="h-details">
	<div class="container">
		<div class="inner_box">
			<div class="row">
				<div class="col-md-12 col-lg-5">
					<div class="item">            
			            <div class="clearfix">
			                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
			                    <li data-thumb="imgs/hotel-d1.jpg"> 
			                        <img src="imgs/hotel-d1.jpg" />
			                    </li>
			                    <li data-thumb="imgs/hotel-d2.jpg"> 
			                        <img src="imgs/hotel-d2.jpg" />
			                    </li>
			                    <li data-thumb="imgs/hotel-d3.jpg"> 
			                        <img src="imgs/hotel-d3.jpg" />
			                    </li>
			                    <li data-thumb="imgs/hotel-d4.jpg"> 
			                        <img src="imgs/hotel-d4.jpg" />
			                    </li>
			                </ul>
			            </div>
			        </div>
				</div>
				<div class="col-md-12 col-lg-7">
					<div class="content">
						<div class="row">
							<div class="col-md-9">
								<h2>Heading Goes Here</h2>
								<div class="star-rating">
									<ul class="list-inline">
										<li class="list-inline-item"><i class="fa fa-star"></i></li>
										<li class="list-inline-item"><i class="fa fa-star"></i></li>
										<li class="list-inline-item"><i class="fa fa-star"></i></li>
										<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
										<li class="list-inline-item"><i class="fa fa-star-o"></i></li>
									</ul>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>
							</div>
							<div class="col-md-3">
								<div class="price_box">
									<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
									<p class="sm-head">per night</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

     <div class="container">
          <div id="availability">
               <h2>Availability</h2>
               <div class="form-inner">
                    <div class="row">
                         <div class="col-md-6 col-lg-3">
                              <div class="box">
                                   <p>Check-in date</p>
                                   <h3><a href="">Mon 7 Dec, 2020</a></h3>
                                   <p class="text-secondary">From 15:00</p>
                              </div>
                         </div>
                         <div class="col-md-6 col-lg-3">
                              <div class="box">
                                   <p>Check-out date</p>
                                   <h3><a href="">Wed 9 Dec, 2020</a></h3>
                                   <p class="text-secondary">From 15:00</p>
                              </div>
                         </div>
                         <div class="col-md-6 col-lg-3">
                              <div class="box">
                                   <p>Guests</p>
                                   <h3><a href="">2 Adults</a></h3>
                              </div>
                         </div>
                         <div class="col-md-6 col-lg-3">
                              <div class="box btbn">
                                   <a href="" class="btn btn-ch-search">Change Search</a>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>

	<div class="container">
		<ul class="nav nav-pills" id="myTab" role="tablist">
  			<li class="nav-item" role="presentation"><a class="nav-link active" id="abouth-tab" data-toggle="tab" href="#abouth" role="tab" aria-controls="abouth" aria-selected="true">About Hotel</a></li>
  			<li class="nav-item" role="presentation"><a class="nav-link" id="rooms-tab" data-toggle="tab" href="#rooms" role="tab" aria-controls="rooms" aria-selected="false">Rooms</a></li>
  			<li class="nav-item" role="presentation"><a class="nav-link" id="facilities-tab" data-toggle="tab" href="#facilities" role="tab" aria-controls="facilities" aria-selected="false">Facilities</a></li>
		</ul>
		<div class="tab-content" id="tabsContent">
  			<div class="tab-pane fade show active" id="abouth" role="tabpanel" aria-labelledby="abouth-tab">
  				<h3>About Hotel Name Here</h3>
  				<p class="text-justify">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>
  			</div>
  			<div class="tab-pane fade" id="rooms" role="tabpanel" aria-labelledby="rooms-tab">
  				<div class="table-responsive">
                    <table class="table table-hover table-bordered">
                    	<tr>
                    		<th width="38%">Room Type</th>
                    		<th width="7%">Sleeps</th>
                    		<th width="15%">Price</th>
                    		<th width="15%">Your Choices</th>
                    		<th width="10%">Select Rooms</th>
                    		<th width="15%"></th>
                    	</tr>
                    	<tr>
                    		<td rowspan="4">
                    			<div class="room_det">
                    				<p><a href="">Superior Double or Twin Room With Free Wifi & 25 Percent off on Food at Selected Restaurant</a></p>
                    				<p class="sm-txt">Only 6 rooms left on our site</p>
                    				<div class="form-group">
                    					<p style="margin-bottom: 0"><b>Choose your bed (if available)</b></p>
                    					<label><input type="radio" name="radio1"> 2 Single Beds </label>
                    					<label><input type="radio" name="radio1"> 1 Double Bed</label>
                    				</div>
                    				<p class="text-secondary" style="margin-bottom: 7px;">This double room features a tile/marble floor, flat-screen TV and electric kettle.</p>
                    				<ul class="list-inline room_facil">
                    					<li><i class="fas fa-city"></i> City View</li>
                    					<li><i class="fas fa-snowflake"></i> Air conditioning</li>
                    					<li><i class="fas fa-bath"></i> Ensuite bathroom</li>
                    					<li><i class="fas fa-tv"></i> Flat-screen TV</li>
                    					<li><i class="fas fa-glass-cheers"></i> Minibar</li>
                    					<li><i class="fas fa-wifi"></i> Free WiFi</li>
                    				</ul>
                    				<hr>
                    				<ul class="list-inline room_facil">
                    					<li><i class="fas fa-check"></i> Free toiletries</li>
                    					<li><i class="fas fa-check"></i> Shower</li>
                    					<li><i class="fas fa-check"></i> Bathrobe</li>
                    					<li><i class="fas fa-check"></i> Safety deposit box</li>
                    					<li><i class="fas fa-check"></i> Toilet</li>
                    					<li><i class="fas fa-check"></i> Towels</li>
                    					<li><i class="fas fa-check"></i> Linen</li>
                    					<li><i class="fas fa-check"></i> Socket near the bed</li>
                    					<li><i class="fas fa-check"></i> Cleaning products</li>
                    					<li><i class="fas fa-check"></i> Tile/marble floor</li>
                    					<li><i class="fas fa-check"></i> Desk</li>
                    					<li><i class="fas fa-check"></i> Seating Area</li>
                    					<li><i class="fas fa-check"></i> TV</li>
                    					<li><i class="fas fa-check"></i> Slippers</li>
                    					<li><i class="fas fa-check"></i> Telephone</li>
                    					<li><i class="fas fa-check"></i> Ironing facilities</li>
                    					<li><i class="fas fa-check"></i> Satellite channels</li>
                    					<li><i class="fas fa-check"></i> Tea/Coffee maker</li>
                    					<li><i class="fas fa-check"></i> Iron</li>
                    					<li><i class="fas fa-check"></i> Hairdryer</li>
                    					<li><i class="fas fa-check"></i> Towels/sheets (extra fee)</li>
                    					<li><i class="fas fa-check"></i> Carpeted</li>
                    					<li><i class="fas fa-check"></i> Electric kettle</li>
                    					<li><i class="fas fa-check"></i> Cable channels</li>
                    					<li><i class="fas fa-check"></i> Wake-up service</li>
                    					<li><i class="fas fa-check"></i> Alarm clock</li>
                    					<li><i class="fas fa-check"></i> Laptop safe</li>
                    					<li><i class="fas fa-check"></i> Wardrobe or closet</li>
                    					<li><i class="fas fa-check"></i> Upper floors accessible by elevator</li>
                    					<li><i class="fas fa-check"></i> Clothes rack</li>
                    					<li><i class="fas fa-check"></i> Drying rack for clothing</li>
                    					<li><i class="fas fa-check"></i> Toilet paper</li>
                    					<li><i class="fas fa-check"></i> Child safety socket covers</li>
                    					<li><i class="fas fa-check"></i> Books, DVDs, or music for children</li>
                    					<li><i class="fas fa-check"></i> Baby safety gates</li>
                    					<li><i class="fas fa-check"></i> Entire unit wheelchair accessible</li>
                    					<li><i class="fas fa-check"></i> Single-room air conditioning for guest accommodation</li>
                    				</ul>
                    			</div>
                    		</td>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>
                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>
                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>
                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td rowspan="6">
                    			<div class="room_det">
                    				<p><a href="">Superior King Room - Non-Smoking With Free Wifi & 25 Percent off on Food at Selected Restaurant</a></p>
                    				<p class="sm-txt">Only 6 rooms left on our site</p>
                    				<div class="form-group">
                    					<p style="margin-bottom: 0"><i class="fas fa-bed"></i> 1 extra-large double bed</p>
                    				</div>
                    				<p class="text-secondary" style="margin-bottom: 7px;">The superior king room features an LED TV satellite channels, minibar, tea/coffee maker, iron and ironing board.</p>
                    				
                    				<ul class="list-inline room_facil">
                    					<li><i class="fas fa-city"></i> City View</li>
                    					<li><i class="fas fa-snowflake"></i> Air conditioning</li>
                    					<li><i class="fas fa-bath"></i> Ensuite bathroom</li>
                    					<li><i class="fas fa-tv"></i> Flat-screen TV</li>
                    					<li><i class="fas fa-glass-cheers"></i> Minibar</li>
                    					<li><i class="fas fa-wifi"></i> Free WiFi</li>
                    				</ul>
                    				<hr>
                    				<a class="text-danger" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">View More</a>
                    				<div class="collapse" id="collapseExample">
	                    				
	                    				<ul class="list-inline room_facil">
	                    					<li><i class="fas fa-check"></i> Free toiletries</li>
	                    					<li><i class="fas fa-check"></i> Shower</li>
	                    					<li><i class="fas fa-check"></i> Bathrobe</li>
	                    					<li><i class="fas fa-check"></i> Safety deposit box</li>
	                    					<li><i class="fas fa-check"></i> Toilet</li>
	                    					<li><i class="fas fa-check"></i> Towels</li>
	                    					<li><i class="fas fa-check"></i> Linen</li>
	                    					<li><i class="fas fa-check"></i> Socket near the bed</li>
	                    					<li><i class="fas fa-check"></i> Cleaning products</li>
	                    					<li><i class="fas fa-check"></i> Tile/marble floor</li>
	                    					<li><i class="fas fa-check"></i> Desk</li>
	                    					<li><i class="fas fa-check"></i> Seating Area</li>
	                    					<li><i class="fas fa-check"></i> TV</li>
	                    					<li><i class="fas fa-check"></i> Slippers</li>
	                    					<li><i class="fas fa-check"></i> Telephone</li>
	                    					<li><i class="fas fa-check"></i> Ironing facilities</li>
	                    					<li><i class="fas fa-check"></i> Satellite channels</li>
	                    					<li><i class="fas fa-check"></i> Tea/Coffee maker</li>
	                    					<li><i class="fas fa-check"></i> Iron</li>
	                    					<li><i class="fas fa-check"></i> Hairdryer</li>
	                    					<li><i class="fas fa-check"></i> Towels/sheets (extra fee)</li>
	                    					<li><i class="fas fa-check"></i> Carpeted</li>
	                    					<li><i class="fas fa-check"></i> Electric kettle</li>
	                    					<li><i class="fas fa-check"></i> Cable channels</li>
	                    					<li><i class="fas fa-check"></i> Wake-up service</li>
	                    					<li><i class="fas fa-check"></i> Alarm clock</li>
	                    					<li><i class="fas fa-check"></i> Laptop safe</li>
	                    					<li><i class="fas fa-check"></i> Wardrobe or closet</li>
	                    					<li><i class="fas fa-check"></i> Upper floors accessible by elevator</li>
	                    					<li><i class="fas fa-check"></i> Clothes rack</li>
	                    					<li><i class="fas fa-check"></i> Drying rack for clothing</li>
	                    					<li><i class="fas fa-check"></i> Toilet paper</li>
	                    					<li><i class="fas fa-check"></i> Child safety socket covers</li>
	                    					<li><i class="fas fa-check"></i> Books, DVDs, or music for children</li>
	                    					<li><i class="fas fa-check"></i> Baby safety gates</li>
	                    					<li><i class="fas fa-check"></i> Entire unit wheelchair accessible</li>
	                    					<li><i class="fas fa-check"></i> Single-room air conditioning for guest accommodation</li>
	                    				</ul>
	                    			</div>
                    			</div>
                    		</td>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td rowspan="2">
                    			<div class="room_det">
                    				<p><a href="">Superior Double or Twin Room With Free Wifi & 25 Percent off on Food at Selected Restaurant</a></p>
                    				<p class="sm-txt">Only 6 rooms left on our site</p>
                    				<div class="form-group">
                    					<p style="margin-bottom: 0"><b>Choose your bed (if available)</b></p>
                    					<label><input type="radio" name="radio1"> 2 Single Beds </label>
                    					<label><input type="radio" name="radio1"> 1 Double Bed</label>
                    				</div>
                    				<p class="text-secondary" style="margin-bottom: 7px;">This double room features a tile/marble floor, flat-screen TV and electric kettle.</p>
                    				<ul class="list-inline room_facil">
                    					<li><i class="fas fa-city"></i> City View</li>
                    					<li><i class="fas fa-snowflake"></i> Air conditioning</li>
                    					<li><i class="fas fa-bath"></i> Ensuite bathroom</li>
                    					<li><i class="fas fa-tv"></i> Flat-screen TV</li>
                    					<li><i class="fas fa-glass-cheers"></i> Minibar</li>
                    					<li><i class="fas fa-wifi"></i> Free WiFi</li>
                    				</ul>
                    				<hr>
                    				<a class="text-danger" data-toggle="collapse" href="#collapseExample1" role="button" aria-expanded="false" aria-controls="collapseExample1">View More</a>
                    				<div class="collapse" id="collapseExample1">
	                    				
	                    				<ul class="list-inline room_facil">
	                    					<li><i class="fas fa-check"></i> Free toiletries</li>
	                    					<li><i class="fas fa-check"></i> Shower</li>
	                    					<li><i class="fas fa-check"></i> Bathrobe</li>
	                    					<li><i class="fas fa-check"></i> Safety deposit box</li>
	                    					<li><i class="fas fa-check"></i> Toilet</li>
	                    					<li><i class="fas fa-check"></i> Towels</li>
	                    					<li><i class="fas fa-check"></i> Linen</li>
	                    					<li><i class="fas fa-check"></i> Socket near the bed</li>
	                    					<li><i class="fas fa-check"></i> Cleaning products</li>
	                    					<li><i class="fas fa-check"></i> Tile/marble floor</li>
	                    					<li><i class="fas fa-check"></i> Desk</li>
	                    					<li><i class="fas fa-check"></i> Seating Area</li>
	                    					<li><i class="fas fa-check"></i> TV</li>
	                    					<li><i class="fas fa-check"></i> Slippers</li>
	                    					<li><i class="fas fa-check"></i> Telephone</li>
	                    					<li><i class="fas fa-check"></i> Ironing facilities</li>
	                    					<li><i class="fas fa-check"></i> Satellite channels</li>
	                    					<li><i class="fas fa-check"></i> Tea/Coffee maker</li>
	                    					<li><i class="fas fa-check"></i> Iron</li>
	                    					<li><i class="fas fa-check"></i> Hairdryer</li>
	                    					<li><i class="fas fa-check"></i> Towels/sheets (extra fee)</li>
	                    					<li><i class="fas fa-check"></i> Carpeted</li>
	                    					<li><i class="fas fa-check"></i> Electric kettle</li>
	                    					<li><i class="fas fa-check"></i> Cable channels</li>
	                    					<li><i class="fas fa-check"></i> Wake-up service</li>
	                    					<li><i class="fas fa-check"></i> Alarm clock</li>
	                    					<li><i class="fas fa-check"></i> Laptop safe</li>
	                    					<li><i class="fas fa-check"></i> Wardrobe or closet</li>
	                    					<li><i class="fas fa-check"></i> Upper floors accessible by elevator</li>
	                    					<li><i class="fas fa-check"></i> Clothes rack</li>
	                    					<li><i class="fas fa-check"></i> Drying rack for clothing</li>
	                    					<li><i class="fas fa-check"></i> Toilet paper</li>
	                    					<li><i class="fas fa-check"></i> Child safety socket covers</li>
	                    					<li><i class="fas fa-check"></i> Books, DVDs, or music for children</li>
	                    					<li><i class="fas fa-check"></i> Baby safety gates</li>
	                    					<li><i class="fas fa-check"></i> Entire unit wheelchair accessible</li>
	                    					<li><i class="fas fa-check"></i> Single-room air conditioning for guest accommodation</li>
	                    				</ul>
	                    			</div>
                    			</div>
                    		</td>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>

                    	<tr>
                    		<td>
                    			<i class="fas fa-user"></i><i class="fas fa-user"></i>
                    		</td>
                    		<td><i class="fas fa-rupee-sign"></i> 7500/-</td>
                    		<td class="text-left">
                    			<p data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><i class="fas fa-mug-hot"></i> Very good breakfast included</p>
                    			<p style="margin-bottom: 0"><i class="fas fa-check"></i> FREE cancellation before 23:59 on 3 December 2020</p>
                    			<p  data-toggle="tooltip" data-placement="bottom" title="No prepayment needed - you pay when you stay at the property"><i class="fas fa-check"></i> NO PREPAYMENT NEEDED - pay at the property</p>
                    		</td>
                    		<td>
                    			<select class="form-control" name="selct_room">
									<option value="0">0</option>
									<option value="1">1 (₹ 7,444)</option>
									<option value="2">2 (₹ 14,888)</option>
								</select>
                    		</td>
                    		<td>
                    			<div class="booking_box sticky-top">
                    				<button type="submit" class="btn btn-reserve" data-toggle="tooltip" data-placement="bottom" title="Select Accomodation First">Book Now</button>
                    			</div>
                    		</td>
                    	</tr>
                    </table>
  				</div>
  			</div>
  			<div class="tab-pane fade" id="facilities" role="tabpanel" aria-labelledby="facilities-tab">
  				<ul class="list-inline room_facil_mn">
					<li><i class="fas fa-swimming-pool"></i> 1 swimming pool</li>
					<li><i class="fas fa-shuttle-van"></i> Airport shuttle</li>
					<li><i class="fas fa-spa"></i> Spa and wellness centre</li>
					<li><i class="fas fa-bread-slice"></i> Room service</li>
					<li><i class="fas fa-smoking-ban"></i> Non-smoking rooms</li>
					<li><i class="fas fa-dumbbell"></i> Fitness Centre</li>
					<li><i class="fas fa-utensils"></i> Restaurant</li>
					<li><i class="fas fa-mug-hot"></i> Tea/coffee maker in all rooms</li>
					<li><i class="fas fa-glass-martini-alt"></i> Bar</li>
					<li><i class="fas fa-bacon"></i> Very good breakfast</li>
				</ul>


  			</div>
		</div>
	</div>
</section>


<section id="house_rule">
     <div class="container">

          <div class="row">
               <div class="col-md-9">
                    <h2>House rules</h2>
                    <p>Crowne Plaza Chennai Adyar Park takes special requests - add in the next step!</p>
               </div>
               <div class="col-md-3">
                    <div class="btns ml-auto">
                         <a href="#" class="btn btn-avail">See availability</a>
                    </div>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="far fa-calendar-alt"></i> Check-in</p>
               </div>
               <div class="col-md-9">
                    <p><span data-component="prc/timebar" class="u-display-block" data-from="15:00" data-from-label="15:00" data-caption="From 15:00 hours">
                         <span class="timebar   ">
                              <span class="timebar__core">
                                   <span class="timebar__base">
                                        <span class="timebar__bar" style="left:62.5%; width:38%"></span>
                                   </span>
                                   <span class="timebar__label" style="left: 62.5%; margin-left: -14.5px;">15:00</span>
                                   <span class="timebar__caption" style="left: 81.5%; margin-left: -57.5px;">From 15:00 hours<span class="timebar__caption-pointer"></span></span>
                              </span>
                         </span>
                    </span></p>
                    <p class="hp-checkin-extra">Guests are required to show a photo identification and credit card upon check-in</p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="far fa-calendar-alt"></i> Check-out</p>
               </div>
               <div class="col-md-9">
                    <p><span data-component="prc/timebar" class="u-display-block" data-from="15:00" data-from-label="15:00" data-caption="From 15:00 hours">
                         <span class="timebar   ">
                              <span class="timebar__core">
                                   <span class="timebar__base">
                                        <span class="timebar__bar" style="left:0%; width:50%"></span>
                                   </span>
                                   <span class="timebar__label" style="left: 50%; margin-left: -14px;">12:00</span>
                                   <span class="timebar__caption" style="left: 25%; margin-left: -55.5px;">Until 12:00 hours<span class="timebar__caption-pointer"></span></span>
                              </span>
                         </span>
                    </span></p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="fas fa-info-circle"></i> Cancellation/prepayment</p>
               </div>
               <div class="col-md-9">
                    <p>Cancellation and prepayment policies vary according to accommodation type. Please check what conditions may apply to each option when making your selection</p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="fas fa-restroom"></i> Children and beds</p>
               </div>
               <div class="col-md-9">
                    <p>Child policies</p>
                    <p style="margin-bottom: 5px;"><b>Children of any age are welcome.</b></p>
                    <p style="margin-bottom: 5px;">Children aged 18 years and above are considered adults at this property.</p>
                    <p style="margin-bottom: 5px;">To see correct prices and occupancy information, please add the number of children in your group and their ages to your search.</p>
                    <p style="margin-bottom: 5px;"><b>Cot and extra bed policies</b></p>
                    <p style="margin-bottom: 5px;">There is no capacity for cots at this property.</p>
                    <p style="margin-bottom: 5px;">There is no capacity for extra beds at this property.</p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="fas fa-user"></i> No age restriction</p>
               </div>
               <div class="col-md-9">
                    <p>There is no age requirement for check-in</p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="fas fa-paw"></i> Pets</p>
               </div>
               <div class="col-md-9">
                    <p>Pets are not allowed.</p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="fas fa-users"></i> Groups</p>
               </div>
               <div class="col-md-9">
                    <p>When booking more than 6 rooms, different policies and additional supplements may apply.</p>
               </div>
          </div>

          <div class="row">
               <div class="col-md-3">
                    <p><i class="fab fa-cc-amazon-pay"></i> Cards accepted at this hotel</p>
               </div>
               <div class="col-md-9">
                    <img src="imgs/cards.jpg">
                    <p>Crowne Plaza Chennai Adyar Park accepts these cards and reserves the right to temporarily hold an amount prior to arrival.</p>
               </div>
          </div>
     </div>
</section>


<?php include('inc/footer.php'); ?>

