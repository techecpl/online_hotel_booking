

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
	<h1>Room Listings</h1>
</section>


<?php include('inc/search.php'); ?>


<section id="listings">
	<div class="container">
		<ul class="nav nav-pills" id="myTab" role="tablist">
  			<li class="nav-item" role="presentation"><a class="nav-link active" id="economyroom-tab" data-toggle="tab" href="#economyroom" role="tab" aria-controls="economyroom" aria-selected="true">Economy Rooms</a></li>
  			<li class="nav-item" role="presentation"><a class="nav-link" id="semideluxeroom-tab" data-toggle="tab" href="#semideluxeroom" role="tab" aria-controls="semideluxeroom" aria-selected="false">Semi Deluxe Rooms</a></li>
  			<li class="nav-item" role="presentation"><a class="nav-link" id="deluxeroom-tab" data-toggle="tab" href="#deluxeroom" role="tab" aria-controls="deluxeroom" aria-selected="false">Deluxe Rooms</a></li>
  			<li class="nav-item" role="presentation"><a class="nav-link" id="honeymoon-tab" data-toggle="tab" href="#honeymoon" role="tab" aria-controls="honeymoon" aria-selected="false">Honeymoon Sweets</a></li>
		</ul>
		<div class="tab-content" id="tabsContent">
  			<div class="tab-pane fade show active" id="economyroom" role="tabpanel" aria-labelledby="economyroom-tab">
  				<h3>Economy Rooms</h3>
  				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/economy1.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/economy2.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/economy3.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/economy4.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/economy5.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
  			</div>
  			<div class="tab-pane fade" id="semideluxeroom" role="tabpanel" aria-labelledby="semideluxeroom-tab">
  				<h3>Semi Deluxe Rooms</h3>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/semideluxe.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/semideluxe1.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
  			</div>
  			<div class="tab-pane fade" id="deluxeroom" role="tabpanel" aria-labelledby="deluxeroom-tab">
  				<h3>Deluxe Rooms</h3>
  				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/deluxe1.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/deluxe2.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/deluxe3.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
  			</div>
  			<div class="tab-pane fade" id="honeymoon" role="tabpanel" aria-labelledby="honeymoon-tab">
  				<h3>Honeymoon Suits</h3>
  				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/honeymoon1.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/honeymoon2.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
				<div class="inner_box">
					<div class="row">
						<div class="col-md-12 col-lg-4">
							<div class="img-hover-zoom"><img src="imgs/honeymoon3.jpg" class="img-fluid"></div>
						</div>
						<div class="col-md-12 col-lg-8">
							<div class="content">
								<div class="row">
									<div class="col-md-9">
										<h2>Heading Goes Here</h2>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

										<ul class="large">
											<li><i class="fas fa-wifi"></i></li>
											<li><i class="fas fa-female"></i></li>
											<li><i class="fas fa-bath"></i></li>
											<li><i class="fas fa-utensils"></i></li>
											<li><i class="fas fa-coffee"></i></li>
											<li><i class="fas fa-glass-martini"></i></li>
											<li><i class="fas fa-mobile-alt"></i></li>
											<li><i class="fas fa-tv"></i></li>
											<li><i class="fas fa-snowflake"></i></li>
										</ul>
									</div>
									<div class="col-md-3">
										<div class="price_box">
											<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
											<p class="sm-head">per night</p>
											<a href="hotel-details.php" class="btn btn-details">More Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</div>
  			</div>
		</div>
	</div>
</section>



<section id="listings">
	<div class="container">
		
	</div>
</section>


<?php include('inc/footer.php'); ?>

