

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
	<h1>Hotel Listings</h1>
</section>


<?php include('inc/search.php'); ?>


<section id="thanks">
	<div class="container">
		<h3>Thanks for Booking</h3>
		<img src="imgs/booking.png" alt="" class="img-fluid">
	</div>
</section>


<!-- <section id="not_found">
	<div class="container">
		<h3>Sorry! No Hotel Found</h3>
		<img src="imgs/notfound.jpg" alt="" class="img-fluid">
	</div>
</section> -->

<section id="listings">
	<div class="container">
		<div class="inner_box">
			<div class="row">
				<div class="col-md-12 col-lg-4">
					<div class="img-hover-zoom"><img src="imgs/room.jpg" class="img-fluid"></div>
				</div>
				<div class="col-md-12 col-lg-8">
					<div class="content">
						<div class="row">
							<div class="col-md-9">
								<h2>Heading Goes Here</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

								<ul class="large">
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
									<li><i class="fas fa-glass-martini"></i></li>
									<li><i class="fas fa-mobile-alt"></i></li>
									<li><i class="fas fa-tv"></i></li>
									<li><i class="fas fa-snowflake"></i></li>
								</ul>
							</div>
							<div class="col-md-3">
								<div class="price_box">
									<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
									<p class="sm-head">per night</p>
									<a href="hotel-details.php" class="btn btn-details">More Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="inner_box">
			<div class="row">
				<div class="col-md-12 col-lg-4">
					<div class="img-hover-zoom"><img src="imgs/room.jpg" class="img-fluid"></div>
				</div>
				<div class="col-md-12 col-lg-8">
					<div class="content">
						<div class="row">
							<div class="col-md-9">
								<h2>Heading Goes Here</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

								<ul class="large">
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
									<li><i class="fas fa-glass-martini"></i></li>
									<li><i class="fas fa-mobile-alt"></i></li>
									<li><i class="fas fa-tv"></i></li>
									<li><i class="fas fa-snowflake"></i></li>
								</ul>
							</div>
							<div class="col-md-3">
								<div class="price_box">
									<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
									<p class="sm-head">per night</p>
									<a href="hotel-details.php" class="btn btn-details">More Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="inner_box">
			<div class="row">
				<div class="col-md-12 col-lg-4">
					<div class="img-hover-zoom"><img src="imgs/room.jpg" class="img-fluid"></div>
				</div>
				<div class="col-md-12 col-lg-8">
					<div class="content">
						<div class="row">
							<div class="col-md-9">
								<h2>Heading Goes Here</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

								<ul class="large">
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
									<li><i class="fas fa-glass-martini"></i></li>
									<li><i class="fas fa-mobile-alt"></i></li>
									<li><i class="fas fa-tv"></i></li>
									<li><i class="fas fa-snowflake"></i></li>
								</ul>
							</div>
							<div class="col-md-3">
								<div class="price_box">
									<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
									<p class="sm-head">per night</p>
									<a href="hotel-details.php" class="btn btn-details">More Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="inner_box">
			<div class="row">
				<div class="col-md-12 col-lg-4">
					<div class="img-hover-zoom"><img src="imgs/room.jpg" class="img-fluid"></div>
				</div>
				<div class="col-md-12 col-lg-8">
					<div class="content">
						<div class="row">
							<div class="col-md-9">
								<h2>Heading Goes Here</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

								<ul class="large">
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
									<li><i class="fas fa-glass-martini"></i></li>
									<li><i class="fas fa-mobile-alt"></i></li>
									<li><i class="fas fa-tv"></i></li>
									<li><i class="fas fa-snowflake"></i></li>
								</ul>
							</div>
							<div class="col-md-3">
								<div class="price_box">
									<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
									<p class="sm-head">per night</p>
									<a href="hotel-details.php" class="btn btn-details">More Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<div class="inner_box">
			<div class="row">
				<div class="col-md-12 col-lg-4">
					<div class="img-hover-zoom"><img src="imgs/room.jpg" class="img-fluid"></div>
				</div>
				<div class="col-md-12 col-lg-8">
					<div class="content">
						<div class="row">
							<div class="col-md-9">
								<h2>Heading Goes Here</h2>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>

								<ul class="large">
									<li><i class="fas fa-wifi"></i></li>
									<li><i class="fas fa-female"></i></li>
									<li><i class="fas fa-bath"></i></li>
									<li><i class="fas fa-utensils"></i></li>
									<li><i class="fas fa-coffee"></i></li>
									<li><i class="fas fa-glass-martini"></i></li>
									<li><i class="fas fa-mobile-alt"></i></li>
									<li><i class="fas fa-tv"></i></li>
									<li><i class="fas fa-snowflake"></i></li>
								</ul>
							</div>
							<div class="col-md-3">
								<div class="price_box">
									<h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>
									<p class="sm-head">per night</p>
									<a href="hotel-details.php" class="btn btn-details">More Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</section>


<?php include('inc/footer.php'); ?>

