<?php include('inc/head.php'); ?>

       <?php include('inc/sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include('inc/header.php'); ?>

                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Form 2</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Form 2</li>
                            </ol>
                        </nav>
                    </div>

                    <div id="addHotels">
                        <form>
                            <div class="addHotel_inner">
                                <div class="row">
                                    <div class="col-md-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="hotelname">Hotel Name</label>
                                            <input type="text" class="form-control" placeholder="Username" />
                                        </div>
                                        <div class="form-group">
                                            <label for="haddress">Address</label>
                                            <input type="text" name="haddress" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="checkout">Checkout</label>
                                            <input type="datetime-local" name="checkout" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="checkin">Checkin</label>
                                            <input type="datetime-local" name="checkin" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-4">
                                        <div class="form-group">
                                            <label for="uploadimage">Upload Image</label>
                                            <input type="file" name="uploadimage" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="dummy">Lorum ipsum</label>
                                            <input type="text" name="dummy" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea name="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-warning">Submit</button>
                                            <button type="submit" class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
      

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           <?php include('inc/footer.php'); ?>  