<?php include('inc/head.php'); ?>



<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column" style="background-color: transparent;">

    <!-- Main Content -->
    <div id="content">



        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


            <div id="register">
                <form>
                    <div class="login register">

                        <div class="logo">
                            <img src="imgs/logo_admin.png">
                        </div>

                        <h3>Login</h3>

                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="password" name="upasswordsername" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Login Now</button>
                        </div>
                    </div>
                    <div class="copyright">Copyright Hotels 2020</div>
                </form>
                  
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->
<!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="src/image-uploader.js"></script>
    <script>
        $('.input-images-1').imageUploader();
    </script>