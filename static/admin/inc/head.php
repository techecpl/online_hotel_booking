<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Hotels</title>

<!-- Custom styles for this template-->
<link href="css/sb-admin-2.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/admin.css">

<link rel="stylesheet" media="screen and (max-width: 1200px)" href="css/admin_resp.css">
<link rel="stylesheet" type="text/css" href="src/image-uploader.css">
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">



</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">