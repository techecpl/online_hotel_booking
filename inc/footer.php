<footer>
    <div class="container">
        <p>Copyright Hilton Hotels 2020. <a href="#">Privacy Policy</a></p>
    </div>
</footer>

<script src="js/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
<script src="js/bootstrap.min.js"></script>
<script src="src/image-uploader.js"></script>
<!--<script src="js/boostrap-select/css/bootstrap-select.min.css"></script>-->
<!--<script src="js/boostrap-select/js/bootstrap-select.min.js"></script>-->


<script src="jquery-ui/jquery-ui.js"></script>
<link rel="stylesheet" href=
"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css"/>
<script src=
        "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<script>
    $(document).ready(function(){
        if($('.fromDate').val()!=''){
            $('.fromDate, .toDate').trigger('change');
        }
    });
    //Datepicker
    var dateFormat = 'dd-mm-yy';
    from = $(".fromDate")
        .datepicker({
            // defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            minDate: 0,
            dateFormat: 'dd-mm-yy',
        })
        .on("change", function () {
            to.datepicker("option", "minDate", getDate(this));
        }),
        to = $(".toDate").datepicker({
            // defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            minDate: 0,
            dateFormat: 'dd-mm-yy',
        })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

    $( function() {
        $( "#datepicker" ).datepicker({minDate: 0});
        $( ".datepicker" ).datepicker({minDate: 0});
        $( "#datepicker2" ).datepicker();
    });

    $(window).scroll(function(){
        $('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
    });
</script>

<script src="src/js/lightslider.js"></script>
<script>
    $(document).ready(function() {
        $('image-gallery').lightSlider({
            gallery:true,
            item:1,
            thumbItem:9,
            slideMargin: 0,
            speed:500,
            auto:true,
            loop:true,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>
<script>
    $('.input-images-1').imageUploader();
</script>
<script>
    var state_id = $('#state_id option:selected').val();
    if(state_id){
        changeContentDropdown(state_id);
    }
    $('#state_id').on("change",function(){
        var state_id = $('#state_id option:selected').val();
        changeContentDropdown(state_id);
    });
    function changeContentDropdown(state) {
        $(".selectpicker#city_id").html('');
        $.ajax({
            url: "<?php echo FRONTEND_ROUTE.'get_cities.php'; ?>",
            type:"POST",
            data: {state : state},
            dataType : "json",
            success: function(data){
                if(data){
                    var selectedCityId = "<?php echo (isset($_SESSION['bookingSearchDetails']['city_id']) && !empty($_SESSION['bookingSearchDetails']['city_id'])) ? $_SESSION['bookingSearchDetails']['city_id'] : '' ?>";
                    var options= [];
                    // $("#city_id").append("<option value=''>Select City</option>");
                    $('.selectpicker#city_id').html("<option value=''>Select City</option>");
                    $.map(data,function(val,i){

                        var city_name = val.name;
                        var selected = (selectedCityId == val.id)?"selected=selected":"";
                        var option = "<option  "+selected+"   value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>";
                        options.push(option);
                        // $("#city_id").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                    $('.selectpicker#city_id').append(options);
                    $('.selectpicker#city_id').selectpicker('refresh');
                }

            }
        });
    }
</script>
</body>
</html>