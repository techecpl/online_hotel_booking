<?php
session_start();
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

if(!isset($_SESSION['user_session']['role_id'] )){
    redirect(FRONTEND_ROUTE . 'index.php');
}
if (!isset($_SESSION['roombooking_cart'])) {
    redirect(FRONTEND_ROUTE . 'index.php');
}

if (isset($_POST['payment_submit'])) {
    $id_field="";
    if($_SESSION['user_session']['role_id'] == TRAVEL_AGENT_ROLE_ID){
        $id_field = ", travel_agent_id ='". $_SESSION['user_session']['travel_agent_id'] ."', customer_id ='". $_SESSION['user_session']['new_customer_id'] ."', booking_by =1";
    }elseif ($_SESSION['user_session']['role_id'] == CUSTOMER_ROLE_ID){
        $id_field = ", customer_id ='". $_SESSION['user_session']['customer_id'] ."'";
    }

    foreach ($_SESSION['roombooking_cart'] as $roomDetails) {

        $booking = $sqlQuery->InsertQuery("INSERT INTO bookings SET check_in ='". $roomDetails['checkin'] ."',check_out ='". $roomDetails['checkout'] ."', room_id ='". $roomDetails['roomid'] ."', hotel_id ='". $roomDetails['hotelid'] ."', date_of_booking ='".date('Y-m-d h:i:s') ."', guest ='". $roomDetails['guest'] ."',kids ='". $roomDetails['kids'] ."', no_of_days ='". $roomDetails['day'] ."', room_charges ='". $roomDetails['roomprice'] ."' {$id_field}");


    }
//        @$tot += $_SESSION['roombooking_cart'][$i]['roomprice'];
    unset($_SESSION['user_session']['new_customer_id']);
    unset($_SESSION['bookingSearchDetails']);
    unset($_SESSION['roombooking_cart']);
    unset($_SESSION['pay']);
    echo "<script>alert('Booking is successfully submitted!');</script>";
        redirect(FRONTEND_ROUTE . "thanks.php");
}
?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
    <h1>Payment</h1>
</section>

<section id="payment">
    <div class="container">
        <div class="pay_main">
            <div class="">
                <h2>Fill the card detail to pay</h2>

                <article class="card">
                    <div class="card-body p-5">

                        <ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
                                    <i class="fa fa-credit-card"></i> Credit Card/Debit Card</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="nav-tab-card">
                                <!-- <p class="alert alert-success">Some text success or error</p> -->
                                <form method="post">
                                    <div class="form-group">
                                        <label for="username">Full name (on the card)</label>
                                        <input type="text" class="form-control" name="username" placeholder=""
                                               required="">
                                    </div> <!-- form-group.// -->

                                    <div class="form-group">
                                        <label for="cardNumber">Card number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="cardNumber" placeholder="">
                                            <div class="input-group-append">
											<span class="input-group-text text-muted">
												<i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>  
												<i class="fab fa-cc-mastercard"></i>
											</span>
                                            </div>
                                        </div>
                                    </div> <!-- form-group.// -->

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label><span class="hidden-xs">Expiration</span> </label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" placeholder="MM" name="">
                                                    <input type="number" class="form-control" placeholder="YY" name="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label data-toggle="tooltip" title=""
                                                       data-original-title="3 digits code on back side of the card">CVV
                                                    <i class="fa fa-question-circle"></i></label>
                                                <input type="number" class="form-control" required="">
                                            </div> <!-- form-group.// -->
                                        </div>
                                    </div> <!-- row.// -->
                                    <div class="text-center">
                                        <button class="subscribe btn btn-primary" name="payment_submit" type="submit">
                                            Confirm
                                        </button>
                                    </div>
                                </form>
                            </div> <!-- tab-pane.// -->
                        </div> <!-- tab-content .// -->
                    </div> <!-- card-body.// -->
                </article> <!-- card.// -->

            </div>
        </div>
    </div>
</section>

<?php include('inc/footer.php'); ?>

