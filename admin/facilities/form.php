<?php
session_start();
require_once("../validate_user.php");
include("../../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

//if(isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5  ){
//    unset($_SESSION['s_time']);
//    unset($_SESSION['u_time']);
//}

if (isset($_POST['facilitySubmit'])) {
//print_r($_POST);
//exit();
    $_SESSION['s_time'] = time();
    $result = $sqlQuery->InsertQuery("INSERT INTO facilities SET name = '" . $_POST['name'] . "', type = '" . $_POST['type'] . "', icon = '" . $_POST['icon'] . "'");
    if (!empty($result)) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Recorded Added Successfully';

    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be added';
    }
    header('Location:index.php');
}
if (isset($_POST['facilityUpdate'])) {
    $id = $_POST['Id'];
    $result = $sqlQuery->UpdateQuery("UPDATE facilities SET name='" . $_POST['name'] . "', type='" . $_POST['type'] . "', icon='" . $_POST['icon'] . "' WHERE facility_id='" . $id . "' ");
    if ($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record updated successfully';
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be updated';
    }

    header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
    $result = array();
    $result ['data'] = $sqlQuery->SelectSingle("SELECT * FROM facilities WHERE facility_id ='" . $_GET['Id'] . "'");
    $editResult = $result['data'];
}
?>
<?php include('../includes/head.php'); ?>

<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Facility</h1>
    </div>

    <div id="addHotels">
        <form method="post">
            <div class="addHotel_inner">
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="form-group">
                            <label for="name">Facility Name</label>
                            <input type="text" class="form-control" id="name" placeholder="facility name"
                                   name="name"
                                   value="<?php echo(isset($editResult['name']) && !empty($editResult['name']) ? $editResult['name'] : '') ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select type="text" name="type" id="type" class="form-control" required>
                                <option value="">Select type</option>
                                <?php foreach ($facilityType as $key => $data) { ?>
                                    <option value="<?php echo $key; ?>" <?php echo (isset($editResult['type']) && $editResult['type'] == $key) ? 'selected' : '' ?>><?php echo $data ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="icon">Icon Name</label>
                            <input type="text" id="icon" name="icon" class="form-control"
                                   value="<?php echo(isset($editResult['icon']) && !empty($editResult['icon']) ? $editResult['icon'] : '') ?>"required />
                        </div>
                    </div>
                    <!--                                <div class="col-md-12 col-lg-4">-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="checkout">Checkout</label>-->
                    <!--                                        <input type="text" name="checkout" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="checkin">Checkin</label>-->
                    <!--                                        <input type="datetime-local" name="checkin" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--                                <div class="col-md-12 col-lg-4">-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="uploadimage">Upload Image</label>-->
                    <!--                                        <input type="file" name="uploadimage" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="uploadimage">Upload Image</label>-->
                    <!--                                        <input type="file" name="uploadimage" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                                echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='facilityUpdate' class='btn btn-primary'>Update</button>";
                            } else {
                                echo "<button type='submit' name='facilitySubmit' class='btn btn-warning'>Submit</button>";
                            }
                            ?>
                            <button type="submit" onclick="window.history.go(-1); return false;"
                                    class="btn btn-secondary">Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


</div>
<!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>  