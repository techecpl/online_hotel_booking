<?php
include("../../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
if (isset($_POST['facilitySubmit'])) {
//print_r($_POST);
//exit();
    $result = $sqlQuery->InsertQuery("INSERT INTO facilities SET name = '" . $_POST['name'] . "', type = '" . $_POST['type'] . "', icon = '" . $_POST['icon'] . "'");
    if (!empty($result)) {
        $message = 'Recorded Added Successfully';
    } else {
        $message = "Record cannot be added";
    }
}
if(isset($_POST['facilityUpdate'])){
    $id = $_POST['Id'];
    $result = $sqlQuery->UpdateQuery("UPDATE facilities SET name='".$_POST['name']."', type='".$_POST['type']."', icon='".$_POST['icon']."' WHERE facility_id='".$id."' ");
    if ($result == true){
        $message = "Record updated successfully";
    }else{
        $message = "Record cannot be updated";
    }

}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
    $action = 'fetch_record';
    $form_data = array('Id' => $_GET['Id']);
    $result  = array();
    $result ['data'] = $sqlQuery->SelectSingle("SELECT * FROM facilities WHERE facility_id ='" . $_GET['Id'] . "'");
    $editResult = $result['data'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include(ADMIN_DOCUMENT_ROUTE.'inc/head.php') ?>


</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <?php include('../inc/sidebar.php'); ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include('../inc/header.php'); ?>

            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Add Facility</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Form 2</li>
                        </ol>
                    </nav>
                </div>

                <div id="addHotels">
                    <form method="post">
                        <div class="addHotel_inner">
                            <div class="row">
                                <div class="col-md-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="name">Facility Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="facility name"
                                               name="name"
                                               value="<?php echo(isset($editResult['name']) && !empty($editResult['name']) ? $editResult['name'] : '') ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                        <select type="text" name="type" id="type" class="form-control">
                                            <option value="">Select type</option>
                                            <?php foreach ($facilityType as $key => $data) { ?>
                                                <option value="<?php echo $key; ?>" <?php echo (isset($editResult['type']) && $editResult['type'] == $key) ? 'selected' : '' ?>><?php echo $data ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="icon">Icon Name</label>
                                        <input type="text" id="icon" name="icon" class="form-control"
                                               value="<?php echo(isset($editResult['icon']) && !empty($editResult['icon']) ? $editResult['icon'] : '') ?>">
                                    </div>
                                </div>
                                <!--                                <div class="col-md-12 col-lg-4">-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <label for="checkout">Checkout</label>-->
                                <!--                                        <input type="text" name="checkout" class="form-control">-->
                                <!--                                    </div>-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <label for="checkin">Checkin</label>-->
                                <!--                                        <input type="datetime-local" name="checkin" class="form-control">-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-md-12 col-lg-4">-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <label for="uploadimage">Upload Image</label>-->
                                <!--                                        <input type="file" name="uploadimage" class="form-control">-->
                                <!--                                    </div>-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <label for="uploadimage">Upload Image</label>-->
                                <!--                                        <input type="file" name="uploadimage" class="form-control">-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                                            echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='facilityUpdate' class='btn btn-primary'>Update</button>";
                                        } else {
                                            echo "<button type='submit' name='facilitySubmit' class='btn btn-warning'>Submit</button>";
                                        } ?>
                                        <button type="submit"  onclick="window.history.go(-1); return false;" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright Hotels 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include(ADMIN_DOCUMENT_ROUTE.'/inc/foot.php') ?>

</body>

</html>