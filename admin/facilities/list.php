<?php
session_start();
require_once("../validate_user.php");
//include "../database/DBConnection.php";
include ($_SERVER['DOCUMENT_ROOT'].ROOT_PATH.'/constants.php');
$query = new SqlQueries();
if(isset($_SESSION['d_time']) && (time() - $_SESSION['d_time'] )> 5){
    session_unset();
}
$records = $query->SelectQuery('SELECT * FROM facilities');
if (isset($_GET['action']) && $_GET['action'] == 'delete'){
    $result = $query->DeleteQuery("DELETE FROM facilities WHERE facility_id='".$_GET['Id']."' ");
    $_SESSION['d_time'] = time();
    if($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record deleted successfully';
        header('Location:list.php');
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be deleted';
        header('Location:list.php');
    }
}
if(isset($_POST['facilitySearch'])){
    $where = '';
    $type = (isset($_POST[ 'facilityType' ]) && !empty($_POST[ 'facilityType' ]) ) ?$_POST[ 'facilityType' ] :'' ;
    if ( $type != '' ) {
        $where .= " and (type like '%" . $type . "%')";
    }
$result = $query->SelectQuery("SELECT * FROM facilities WHERE name LIKE '%".$_POST['faciltityName']."%' {$where} ");
    $records = $result;
}
//print_r($records);
//exit();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include(ADMIN_DOCUMENT_ROUTE.'inc/head.php') ?>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <?php include('../inc/sidebar.php'); ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include('../inc/header.php'); ?>

            <!-- End of Topbar -->

            <!-- Begin Page Content -->

                <div class="container-fluid">
                    <?php if(isset($_SESSION['level'])){?>
                    <div class="alert alert-<?php echo $_SESSION['level'];?> alert-dismissible fade show" role="alert"> <?php echo $_SESSION['message'];?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php } ?>
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Facility List</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Form 2</li>
                            </ol>
                        </nav>
                    </div>

                    <div id="listings">

                        <div id="searchForm">
                            <form method="post" name="facilitySearchForm" id="facilitySearchForm">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="form-group">
                                            <input type="text" name="faciltityName" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="form-group">
                                            <select name="facilityType" class="form-control">
                                                <option value="">---Select---</option>
                                                <?php foreach ($facilityType as $key => $data) { ?>
                                                    <option value="<?php echo $key; ?>" <?php echo (isset($_POST['facilityType']) && $_POST['facilityType'] == $key) ? 'selected' : '' ?>><?php echo $data ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="form-group">
                                            <button class="btn btn-warning" name="facilitySearch" type="submit">Search</button>
                                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th scope="col">Sr. No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Icon</th>
                                    <th scope="col">Action</th>
                                </tr>
                                <?php
                                $no = 1;
                                if(isset($records) && count($records) > 0 ){
                                    foreach ($records as $record) {?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $record['name']?></td>
                                            <td><?php echo $facilityType[$record['type']]?></td>
                                            <td><?php echo $record['icon']?></td>
                                            <td>
                                                <a href="<?php echo ADMIN_ROUTE?>"><img src="<?php echo ADMIN_ROUTE?>imgs/view.jpg" width="22"></a>
                                                <a href="<?php echo ADMIN_ROUTE?>facilities/index.php?action=edit&Id=<?php echo $record['facility_id']?>" style="margin:0 5px;"><img src="<?php echo ADMIN_ROUTE?>imgs/edit.jpg" width="22"></a>
                                                <a href="list.php?action=delete&Id=<?php echo $record['facility_id']?>"><img src="<?php echo ADMIN_ROUTE?>imgs/delete.jpg" width="22"></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }else {
                                            echo "    <tr>
                                    <td colspan='5'>No records found</td>
                                </tr>";
                                    }?>
                            </table>
                        </div>
                    </div>


                </div>

            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright Hotels 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>


<!-- Bootstrap core JavaScript-->

<script src="<?php echo ADMIN_ROUTE?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo ADMIN_ROUTE?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo ADMIN_ROUTE?>assets/js/sb-admin-2.min.js"></script>
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("facilitySearchForm").reset();
    }
    function resetFormOnclick() {
        document.getElementById("facilitySearchForm").reset();
        window.location='list.php';
    }

</script>
</body>

</html>