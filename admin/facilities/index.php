<?php
session_start();
require_once("../validate_user.php");
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();
//if (isset($_SESSION['d_time']) && (time() - $_SESSION['d_time'] ) > 5 || isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] ) > 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] ) > 5) {
//    session_destroy();
//}
$records = $query->SelectQuery('SELECT * FROM facilities');
if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    $result = $query->DeleteQuery("DELETE FROM facilities WHERE facility_id='" . $_GET['Id'] . "' ");
    $_SESSION['d_time'] = time();
    if ($result == true) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record deleted successfully';
        header('Location:index.php');
    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be deleted';
        header('Location:index.php');
    }
}
if (isset($_POST['facilitySearch'])) {
    $where = '';
    $type = (isset($_POST['facilityType']) && !empty($_POST['facilityType']) ) ? $_POST['facilityType'] : '';
    if ($type != '') {
        $where .= " and (type like '%" . $tpe . "%')";
    }
    $result = $query->SelectQuery("SELECT * FROM facilities WHERE name LIKE '%" . $_POST['faciltityName'] . "%' {$where} ");
    $records = $result;
}
?>
<?php include('../includes/head.php'); ?>
<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Facility List</h1>
    </div>

    <div id="listings">

        <div id="searchForm">
            <form method="post" name="facilitySearchForm" id="facilitySearchForm">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <select name="facilityType" class="form-control">
                                <option value="">---Select Type---</option>
                                <?php foreach ($facilityType as $key => $data) { ?>
                                    <option value="<?php echo $key; ?>" <?php echo (isset($_POST['facilityType']) && $_POST['facilityType'] == $key) ? 'selected' : '' ?>><?php echo $data ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <input type="text" name="faciltityName" placeholder="" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="facilitySearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Icon</th>
                    <th scope="col">Action</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $record['name'] ?></td>
                            <td><?php echo $facilityType[$record['type']] ?></td>
                            <td><?php echo $record['icon'] ?></td>
                            <td>
                                <a href="<?php echo ADMIN_ROUTE ?>facilities/form.php?action=edit&Id=<?php echo $record['facility_id'] ?>" style="margin:0 5px;"><img src="<?php echo ADMIN_ROUTE ?>imgs/edit.jpg" width="22"></a>
                                <a href="index.php?action=delete&Id=<?php echo $record['facility_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/delete.jpg" width="22"></a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='5'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("facilitySearchForm").reset();
    }
    function resetFormOnclick() {
        document.getElementById("facilitySearchForm").reset();
        window.location = 'index.php';
    }

</script>

