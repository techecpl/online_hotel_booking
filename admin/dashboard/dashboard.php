<?php
session_start();
require_once("../validate_user.php");
//include "../database/DBConnection.php";
require_once("../../classes/SqlQueries.php");
?>
<?php include('../includes/head.php'); ?>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Form 2</li>
                </ol>
            </nav>
        </div>

        <div id="listings">

            <div id="searchForm">
                <form>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="form-group">
                                <input type="text" name="dummy" placeholder="Dummy" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="form-group">
                                <select name="dummy" class="form-control">
                                    <option value="">---Select---</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="form-group">
                                <select name="dummy" class="form-control">
                                    <option value="">---Select---</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="form-group">
                                <button class="btn btn-warning" type="submit">Search</button>
                                <button class="btn btn-secondary" type="submit">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th scope="col">Sr. No</th>
                        <th scope="col">Image</th>
                        <th scope="col">Hotel Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Checkin</th>
                        <th scope="col">Checkout</th>
                        <th scope="col">Price</th>
                        <th scope="col">Payment Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>
                            <div class="img-hover-zoom"><img class="hot_img" src="../imgs/hotel_img.jpg"></div>
                        </td>
                        <td>
                            <div class="text-center">
                                <img src="../imgs/user_icon.png"> Dummy Hotel
                            </div>
                        </td>
                        <td>Lorum ipsum dolor</td>
                        <td>28/11/2020</td>
                        <td>30/11/2020</td>
                        <td><span class="text-success">INR 3200/-</span></td>
                        <td><span class="pr_box bg-success">Pending</td>
                        <td>
                            <a href="#"><img src="../imgs/view.jpg" width="22"></a>
                            <a href="#" style="margin:0 5px;"><img src="../imgs/edit.jpg" width="22"></a>
                            <a href="#"><img src="../imgs/delete.jpg" width="22"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>
                            <div class="img-hover-zoom"><img class="hot_img" src="../imgs/hotel_img.jpg"></div>
                        </td>
                        <td>
                            <div class="text-center">
                                <img src="../imgs/user_icon.png"> Dummy Hotel
                            </div>
                        </td>
                        <td>Lorum ipsum dolor</td>
                        <td>28/11/2020</td>
                        <td>30/11/2020</td>
                        <td><span class="text-success">INR 3200/-</span></td>
                        <td><span class="pr_box bg-success">Pending</td>
                        <td>
                            <a href="#"><img src="../imgs/view.jpg" width="22"></a>
                            <a href="#" style="margin:0 5px;"><img src="../imgs/edit.jpg" width="22"></a>
                            <a href="#"><img src="../imgs/delete.jpg" width="22"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>
                            <div class="img-hover-zoom"><img class="hot_img" src="../imgs/hotel_img.jpg"></div>
                        </td>
                        <td>
                            <div class="text-center">
                                <img src="../imgs/user_icon.png"> Dummy Hotel
                            </div>
                        </td>
                        <td>Lorum ipsum dolor</td>
                        <td>28/11/2020</td>
                        <td>30/11/2020</td>
                        <td><span class="text-danger">INR 3200/-</span></td>
                        <td><span class="pr_box bg-danger">Pending</td>
                        <td>
                            <a href="#"><img src="../imgs/view.jpg" width="22"></a>
                            <a href="#" style="margin:0 5px;"><img src="../imgs/edit.jpg" width="22"></a>
                            <a href="#"><img src="../imgs/delete.jpg" width="22"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>
                            <div class="img-hover-zoom"><img class="hot_img" src="../imgs/hotel_img.jpg"></div>
                        </td>
                        <td>
                            <div class="text-center">
                                <img src="../imgs/user_icon.png"> Dummy Hotel
                            </div>
                        </td>
                        <td>Lorum ipsum dolor</td>
                        <td>28/11/2020</td>
                        <td>30/11/2020</td>
                        <td><span class="text-success">INR 3200/-</span></td>
                        <td><span class="pr_box bg-success">Pending</td>
                        <td>
                            <a href="#"><img src="../imgs/view.jpg" width="22"></a>
                            <a href="#" style="margin:0 5px;"><img src="../imgs/edit.jpg" width="22"></a>
                            <a href="#"><img src="../imgs/delete.jpg" width="22"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>
                            <div class="img-hover-zoom"><img class="hot_img" src="../imgs/hotel_img.jpg"></div>
                        </td>
                        <td>
                            <div class="text-center">
                                <img src="../imgs/user_icon.png"> Dummy Hotel
                            </div>
                        </td>
                        <td>Lorum ipsum dolor</td>
                        <td>28/11/2020</td>
                        <td>30/11/2020</td>
                        <td><span class="text-danger">INR 3200/-</span></td>
                        <td><span class="pr_box bg-danger">Pending</td>
                        <td>
                            <a href="#"><img src="../imgs/view.jpg" width="22"></a>
                            <a href="#" style="margin:0 5px;"><img src="../imgs/edit.jpg" width="22"></a>
                            <a href="#"><img src="../imgs/delete.jpg" width="22"></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>


    </div>
    <!-- /.container-fluid -->

    <!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>