<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php if (isset($_SESSION['level'])) { ?>
        <div class="alert alert-<?php echo $_SESSION['level']; ?> alert-dismissible fade show" role="alert"> <?php echo $_SESSION['message']; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Room Type List</h1>
      
    </div>

    <div id="listings">

        <div id="searchForm">
            <form method="post" name="roomTypeSearchForm" id="roomTypeSearchForm">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <input type="text" name="roomTypeTitle" placeholder="" value="<?php echo (isset($_POST['roomTypeTitle']) && !empty($_POST['roomTypeTitle'])) ? $_POST['roomTypeTitle'] : '' ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="roomTypeSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Action</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $record['title'] ?></td>
                            <td><?php echo $record['description'] ?></td>
                            <td>
                                <a href="<?php echo ADMIN_ROUTE ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/view.jpg" width="22"></a>
                                <a href="<?php echo ADMIN_ROUTE ?>roomType/index.php?action=edit&Id=<?php echo $record['room_type_id'] ?>" style="margin:0 5px;"><img src="<?php echo ADMIN_ROUTE ?>imgs/edit.jpg" width="22"></a>
                                <a href="list.php?action=delete&Id=<?php echo $record['room_type_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/delete.jpg" width="22"></a>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='5'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("roomTypeSearchForm").reset();
    }
    function resetFormOnclick() {
        document.getElementById("roomTypeSearchForm").reset();
        window.location = 'list.php';
    }

</script>

