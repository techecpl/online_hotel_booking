<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php if (isset($_SESSION['level'])) { ?>
        <div class="alert alert-<?php echo $_SESSION['level']; ?> alert-dismissible fade show" role="alert"> <?php echo $_SESSION['message']; ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php } ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Room Type</h1>
       
    </div>

    <div id="addHotels">
        <form method="post">
            <div class="addHotel_inner">
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="form-group">
                            <label for="room_title">Room Title</label>
                            <input type="text" class="form-control" id="room_title" placeholder=""
                                   name="room_title"
                                   value="<?php echo(isset($editResult['title']) && !empty($editResult['title']) ? $editResult['title'] : '') ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control"><?php echo(isset($editResult['description']) && !empty($editResult['description']) ? $editResult['description'] : '') ?></textarea>
                        </div>


                    </div>
                    <!--                                <div class="col-md-12 col-lg-4">-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="checkout">Checkout</label>-->
                    <!--                                        <input type="text" name="checkout" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="checkin">Checkin</label>-->
                    <!--                                        <input type="datetime-local" name="checkin" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--                                <div class="col-md-12 col-lg-4">-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="uploadimage">Upload Image</label>-->
                    <!--                                        <input type="file" name="uploadimage" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                    <div class="form-group">-->
                    <!--                                        <label for="uploadimage">Upload Image</label>-->
                    <!--                                        <input type="file" name="uploadimage" class="form-control">-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                                echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='roomTypeUpdate' class='btn btn-primary'>Update</button>";
                            } else {
                                echo "<button type='submit' name='roomTypeSubmit' class='btn btn-warning'>Submit</button>";
                            }
                            ?>
                            <button type="submit"  onclick="window.history.go(-1); return false;" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


</div>
<!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>  