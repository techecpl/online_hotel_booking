<?php
session_start();
require_once("../validate_user.php");
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();
if (isset($_GET['action']) && ($_GET['action'] == 'facility' || $_GET['action'] == 'editFacility')) {
    $hotelId = $_GET['Id'];
}

//if(isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5  ){
//    unset($_SESSION['s_time']);
//    unset($_SESSION['u_time']);
//}
$hotelList = $query->SelectQuery('SELECT hotel_id,name FROM hotels');
$facilityList = $query->SelectQuery('SELECT * FROM facilities WHERE type = 1');

if (isset($_POST['hotelFacilitySubmit'])) {
    $_SESSION['s_time'] = time();
    $facilitiesArray = array();
    $facilitiesArray = $_POST['facility_id'];
    if (isset($facilitiesArray) && count($facilitiesArray) > 0) {
        foreach ( $facilitiesArray as $key => $value) {
    $result =  $query->InsertQuery("INSERT INTO hotel_facilities SET hotel_id ='".$_POST['hotel_id']."', facility_id = '".$value."' " );
            if (!empty($result)) {
                $_SESSION['level'] = 'success';
                $_SESSION['message'] = 'Recorded Added Successfully';
            } else {
                $_SESSION['level'] = 'danger';
                $_SESSION['message'] = 'Record cannot be added';
                header('Location:hotel_facilities.php?action=editFacility&Id='.$hotelId);
            }
        }
        header('Location:hotel_facilities.php?action=editFacility&Id='.$hotelId);
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Facility id not found';
        header('Location:hotel_facilities.php?action=editFacility&Id='.$hotelId);
    }
}

if(isset($_POST['hotelFacilityUpdate'])){
    $_SESSION['u_time'] = time();
    $facilitiesArray = array();
    $facilitiesArray = $_POST['facility_id'];
    if (isset($facilitiesArray) && count($facilitiesArray) > 0) {
        $query->DeleteQuery("DELETE FROM hotel_facilities WHERE hotel_id = '".$hotelId."' ");
        foreach ( $facilitiesArray as $key => $value) {
            $result =  $query->InsertQuery("INSERT INTO hotel_facilities SET hotel_id = '".$hotelId."', facility_id = '".$value."'" );
            if (!empty($result)) {
                $_SESSION['level'] = 'success';
                $_SESSION['message'] = 'Recorded Updated Successfully';
            } else {
                $_SESSION['level'] = 'danger';
                $_SESSION['message'] = 'Record cannot be added';
                header('Location:hotel_facilities.php?action=editFacility&Id='.$hotelId);
            }
        }
        header('Location:hotel_facilities.php?action=editFacility&Id='.$hotelId);
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Facility id not found';
        header('Location:hotel_facilities.php?action=editFacility&Id='.$hotelId);
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'editFacility') {
    $queryResult = $query->SelectQuery("SELECT *,GROUP_CONCAT(facility_id)  as facility_ids FROM hotel_facilities WHERE hotel_id='".$hotelId."' GROUP BY hotel_id");
    $editResult = $queryResult[0];
}
?>
<?php include('../includes/head.php'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <?php include('../includes/alert.php'); ?>
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <?php if(isset($_GET['action']) && $_GET['action'] == 'editFacility') { ?>
            <h1 class="h3 mb-0 text-gray-800">Edit Hotel Facilities</h1>
    <?php }else{ ?>
                <h1 class="h3 mb-0 text-gray-800">Add Hotel Facilities</h1>
            <?php } ?>
        </div>

        <div id="addHotels">
            <form method="post">
                <div class="addHotel_inner">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="form-group">
                                <label for="hotel_id">Hotel</label>
                                <select class="form-control" id="hotel_id" name="hotel_id">
                                    <option value="">Select Hotel</option>
                                    <?php foreach ($hotelList as $data) {
                                        $selected = (isset($hotelId) && !empty($hotelId) && $hotelId == $data['hotel_id']) ? 'selected' : '';
                                        echo "<option value='" . $data['hotel_id'] . "' " . $selected . ">" . $data['name'] . "</option>";
                                    } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="facility_id">Facilities</label>
                                <select class="form-control" id="facility_id" name="facility_id[]" multiple>
                                    <?php
                                    foreach ($facilityList as $data) {
                                        $facilityIds = array();
                                        if(isset($editResult['facility_ids'])){
                                        $facilityIds = explode(',',$editResult['facility_ids']);
                                        }
                                        $selected = (isset($facilityIds) && !empty($facilityIds) && in_array($data['facility_id'],$facilityIds)) ? 'selected' : '';
                                        echo "<option value='" . $data['facility_id'] . "' " . $selected . ">" . $data['name'] . "</option>";
                                    } ?>
                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if (isset($_GET['action']) && $_GET['action'] == "editFacility" && isset($_GET['Id'])) {
                                    echo " <button type='submit' name='hotelFacilityUpdate' class='btn btn-primary'>Update</button>";
                                } else {
                                    echo "<button type='submit' name='hotelFacilitySubmit' class='btn btn-warning'>Submit</button>";
                                }
                                ?>
                                <button type="submit" onclick="window.history.go(-1); return false;"
                                        class="btn btn-secondary">Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>


    </div>
    <!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>