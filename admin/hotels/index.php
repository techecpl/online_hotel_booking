<?php
session_start();
require_once("../validate_user.php");
//include "../database/DBConnection.php";
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();
//if(isset($_SESSION['d_time']) && (time() - $_SESSION['d_time'] )> 5 || isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5 ){
//    session_destroy();
//}
$records = $query->SelectQuery('SELECT hotels.*,states.name as state_name, cities.name as city_name FROM hotels INNER JOIN states ON states.id = hotels.state_id INNER JOIN cities ON cities.id = hotels.city_id');
if (isset($_GET['action']) && $_GET['action'] == 'delete'){
    $checkResult = $query->SelectSingle("SELECT * FROM hotels WHERE hotel_id = '".$_GET['Id']."'");
    $filename = HOTEL_IMAGE_UPLOAD_PATH.$checkResult['image'];
    $result = $query->DeleteQuery("DELETE FROM hotels WHERE hotel_id='".$_GET['Id']."' ");
    if (file_exists($filename)) {
        unlink($filename);
        $response = array('status' => 'success');
    } else {
        $response = array('status' => 'fail');
    }
    $_SESSION['d_time'] = time();
    if($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record deleted successfully';
        header('Location:index.php');
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be deleted';
        header('Location:index.php');
    }
}
if(isset($_POST['hotelSearch'])){
//    $where = '';
//    $type = (isset($_POST[ 'facilityType' ]) && !empty($_POST[ 'facilityType' ]) ) ?$_POST[ 'facilityType' ] :'' ;
//    if ( $type != '' ) {
//        $where .= " and (type like '%" . $type . "%')";
//    }
    $result = $query->SelectQuery("SELECT hotels.*,states.name as state_name, cities.name as city_name FROM hotels INNER JOIN states ON states.id = hotels.state_id INNER JOIN cities ON cities.id = hotels.city_id WHERE hotels.name LIKE '%".$_POST['hotelName']."%' ");
    $records = $result;
}
//print_r($records);
//exit();
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Hotel List</h1>

    </div>

    <div id="listings">

        <div id="searchForm">
            <form method="post" name="hotelSearch" id="hotelSearch">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <input type="text" name="hotelName" placeholder="" value="<?php echo (isset($_POST['hotelName']) && !empty($_POST['hotelName'])) ? $_POST['hotelName'] : '' ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="hotelSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">No Of rooms</th>
                    <th scope="col">Address</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                       $count =  $query->CountQuery("SELECT * FROM hotel_facilities WHERE hotel_id ='".$record['hotel_id']."'");
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><img src="<?php echo FRONTEND_UPLOAD_PATH_ORG.'hotel_images/'.$record['image'] ?>" width="50px" height="50px"/></td>
                            <td><?php echo $record['name'] ?></td>
                            <td><?php echo $hotelType[$record['type']]; ?></td>
                            <td><?php echo $record['no_of_rooms'] ?></td>
                            <td><?php echo $record['locality'].','.$record['city_name'].','.$record['state_name'].' '.$record['zipcode'] ?></td>
                            <td><?php echo $record['contact_no'] ?></td>
                            <td><?php echo $record['email_id'] ?></td>
                            <td>
                                <div  class="row">
                                    <div class="col-4 p-1">
                                <a href="<?php echo ADMIN_ROUTE ?>hotels/form.php?action=edit&Id=<?php echo $record['hotel_id'] ?>" style="margin:0 5px;"><img src="<?php echo ADMIN_ROUTE ?>imgs/edit.jpg" width="22"></a>
                                    </div>
                                    <div class="col-4 p-1">
                                <a href="index.php?action=delete&Id=<?php echo $record['hotel_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/delete.jpg" width="22"></a>
                                    </div>
                                    <?php if($count == 0){?>
                                        <div class="col-4 p-1">
                                            <a href="<?php echo ADMIN_ROUTE ?>hotels/hotel_facilities.php?action=facility&Id=<?php echo $record['hotel_id'] ?>"><i class="text-warning fas fa-plus-square fa-2x"></i> </a>
                                        </div>
                                    <?php } else{?>
                                        <div class="col-4 p-1">
                                            <a href="<?php echo ADMIN_ROUTE ?>hotels/hotel_facilities.php?action=editFacility&Id=<?php echo $record['hotel_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/paper-edit.png" width="22"> </a>
                                        </div>
                                    <?php }?>

                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='5'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("hotelSearch").reset();
    }
    function resetFormOnclick() {
        document.getElementById("hotelSearch").reset();
        window.location = 'index.php';
    }

</script>

