<?php
session_start();
require_once("../validate_user.php");
include("../../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

if (isset($_POST['hotelSubmit'])) {
    $_SESSION['s_time'] = time();
    
    $query = 'INSERT INTO hotels SET name = "' . $_POST["hotelname"]
            . '", type = "' . $_POST["hotelType"] 
            . '", description= "' . $_POST["description"] 
            . '", locality= "' . $_POST["locality"] 
            . '", no_of_rooms= "' . $_POST["no_of_rooms"] 
            . '", state_id= "' . $_POST["state_id"] 
            . '", city_id= "' . $_POST["city_id"] 
            . '", zipcode= "' . $_POST["zipcode"] 
            . '", contact_no= "' . $_POST["contact_no"]
            . '", email_id= "' . $_POST["email_id"] . '"';
 

    $result = $sqlQuery->InsertQuery($query);

    if(isset($_FILES['images']['name']) && !empty($_FILES['images']['name'][0])){
        $response = $sqlQuery->UpdateQuery("UPDATE hotels SET image ='".$result.'-'.$_FILES['images']['name'][0]."' WHERE hotel_id='".$result."' ");
        if (!file_exists(HOTEL_IMAGE_UPLOAD_PATH)) {
            mkdir(HOTEL_IMAGE_UPLOAD_PATH, 0777, true);
        }
        $target_file =HOTEL_IMAGE_UPLOAD_PATH.$result.'-'.basename($_FILES['images']['name'][0]);

        if (move_uploaded_file($_FILES["images"]["tmp_name"][0], $target_file)) {
            echo "The file ". htmlspecialchars( basename( $_FILES['images']['name'][0])). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    if (!empty($result)) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record Added Successfully';

    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be added';
        $message = "Record cannot be added";
    }
    header('Location:index.php');
}
if(isset($_POST['hotelUpdate'])){
    $id = $_POST['Id'];
    $_SESSION['u_time'] = time();
    $query = 'UPDATE hotels SET name = "' . $_POST["hotelname"]
            . '", type = "' . $_POST["hotelType"] 
            . '", description= "' . $_POST["description"] 
            . '", locality= "' . $_POST["locality"] 
            . '", no_of_rooms= "' . $_POST["no_of_rooms"] 
            . '", state_id= "' . $_POST["state_id"] 
            . '", city_id= "' . $_POST["city_id"] 
            . '", zipcode= "' . $_POST["zipcode"] 
            . '", contact_no= "' . $_POST["contact_no"]
            . '", email_id= "' . $_POST["email_id"] 
            . '"  WHERE hotel_id='.$id;
    
   
    $result = $sqlQuery->UpdateQuery($query);

    if(isset($_FILES['images']['name']) && !empty($_FILES['images']['name'][0])){
        $checkResult = $sqlQuery->SelectSingle("SELECT * FROM hotels WHERE hotel_id = '".$id."'");
        $filename = HOTEL_IMAGE_UPLOAD_PATH.$checkResult['image'];
        if (file_exists($filename)) {
            unlink($filename);
            $response = array('status' => 'success');
        } else {
            $response = array('status' => 'fail');
        }
        $response = $sqlQuery->UpdateQuery("UPDATE hotels SET image ='".$id.'-'.$_FILES['images']['name'][0]."' WHERE hotel_id='".$id."' ");
        if (!file_exists(HOTEL_IMAGE_UPLOAD_PATH)) {
            mkdir(HOTEL_IMAGE_UPLOAD_PATH, 0777, true);
        }
        $target_file =HOTEL_IMAGE_UPLOAD_PATH.$id.'-'.basename($_FILES['images']['name'][0]);

        if (move_uploaded_file($_FILES["images"]["tmp_name"][0], $target_file)) {
            echo "The file ". htmlspecialchars( basename( $_FILES['images']['name'][0])). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    if ($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record updated successfully';
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be updated';
    }

    header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
    $result  = array();
    $result ['data'] = $sqlQuery->SelectSingle("SELECT * FROM hotels WHERE hotel_id ='" . $_GET['Id'] . "'");
    $editResult = $result['data'];
}

$states = $sqlQuery->SelectQuery("SELECT * FROM states WHERE country_id='230' ");

?>
<?php include('../includes/head.php'); ?>
    <style>
        .uploaded{
            width: 100%;
        }
        .uploaded .uploaded-image{
            display: inline-block;
            width: 33%;
            padding: 5px;
        }
        .uploaded img
        {
            height: 100px;
            width: 100%;
        }
    </style>
<!-- Begin Page Content -->
    <div class="container-fluid">
        <?php include('../includes/alert.php'); ?>
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Hotel</h1>
        </div>

        <div id="addHotels">
            <form method="post" enctype="multipart/form-data">
                <div class="addHotel_inner">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="hotelname">Hotel Name</label>
                                <input type="text" name="hotelname" class="form-control" value="<?php echo (isset($editResult['name']) && !empty($editResult['name'])) ? $editResult['name'] : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="locality">Locality</label>
                                <input type="text" name="locality" class="form-control" value="<?php echo (isset($editResult['locality']) && !empty($editResult['locality'])) ? $editResult['locality'] : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="state_id">State</label>
                                <select name="state_id" id="state_id" class="form-control" required>
                                    <option value="">Select State</option>
                                    <?php foreach($states as $state){
                                        ?>
                                        <option value="<?php echo $state['id'] ?>" <?php echo (isset($editResult['state_id']) && !empty($editResult['state_id']) && $editResult['state_id'] == $state['id'] ) ? 'selected' : '';?>><?php echo $state['name']?></option>
                                        <?php
                                    }?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="city_id">City</label>
                                <select name="city_id" id="city_id" class="form-control state_city" required>
                                    <option value="">Select City</option>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="zipcode">Zipcode</label>
                                <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo (isset($editResult['zipcode']) && !empty($editResult['zipcode'])) ? $editResult['zipcode'] : ''; ?>" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                           
                            <div class="form-group">
                                <label for="hotelType">Hotel Type</label>
                                <select name="hotelType" id="hotelType" class="form-control" required>
                                    <option value="">Select hotel type</option>
                                    <?php foreach ($hotelType as $key =>$data){
                                        $selected  = (isset($editResult['type']) && !empty($editResult['type']) && $editResult['type'] == $key ) ? 'selected' :'';
                                        echo "<option value='".$key."' ".$selected." >".$data."</option>";
                                    }?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="no_of_rooms">No. of Rooms</label>
                                <input type="number" id="no_of_rooms" name="no_of_rooms" class="form-control" value="<?php echo (isset($editResult['no_of_rooms']) && !empty($editResult['no_of_rooms'])) ? $editResult['no_of_rooms'] : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="contact_no">Contact</label>
                                <input type="number" id="contact_no" name="contact_no" class="form-control" value="<?php echo (isset($editResult['contact_no']) && !empty($editResult['contact_no'])) ? $editResult['contact_no'] : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="email_id">Email</label>
                                <input type="email" id="email_id" name="email_id" class="form-control" value="<?php echo (isset($editResult['email_id']) && !empty($editResult['email_id'])) ? $editResult['email_id'] : ''; ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="uploadimage">Upload Image</label>
                                <div class="input-images-1"></div>
                                <?php if(isset($editResult['image']) && !empty($editResult['image'])){?>
                                    <div class="uploaded"><div class="uploaded-image" data-index="1"><img src="<?php echo FRONTEND_UPLOAD_PATH_ORG.'hotel_images/'.$editResult['image'];?>"></div></div>
                                <?php }?>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control"><?php echo (isset($editResult['description']) && !empty($editResult['description'])) ? $editResult['description'] : ''; ?></textarea>
                            </div>
                        </div>
                    </div>
                    
                     
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                                    echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='hotelUpdate' class='btn btn-primary'>Update</button>";
                                } else {
                                    echo "<button type='submit' name='hotelSubmit' class='btn btn-warning'>Submit</button>";
                                }
                                ?>
                                <button type="submit"  onclick="window.history.go(-1); return false;" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>


    </div>
<!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>
<script>
    var state_id = $('#state_id option:selected').val();
    if(state_id){
        changeContentDropdown(state_id);
    }
    $('#state_id').on("change",function(){
        var state_id = $('#state_id option:selected').val();
        changeContentDropdown(state_id);
    });
    function changeContentDropdown(state) {
        $(".state_city").html('');
        $.ajax({
            url: "<?php echo ADMIN_ROUTE.'hotels/get_state_city.php'; ?>",
            type:"POST",
            data: {state : state},
            dataType : "json",
            success: function(data){
                if(data){
                    var selectedCityId = "<?php echo (isset($editResult['city_id']) && !empty($editResult['city_id'])) ? $editResult['city_id'] : '' ?>";
                    // var selectedCityId = "  ";

                    $(".state_city").append("<option value=''>Select City</option>");
                    $.map(data,function(val,i){

                        var city_name = val.name;
                        var selected = (selectedCityId == val.id)?"selected=selected":"";
                        // console.log(selected);
                        // var d = (content==val.ContentId)?'selected="selected"' : '';
                        $(".state_city").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                }

            }
        });
    }
</script>
