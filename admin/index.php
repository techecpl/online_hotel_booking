<?php
session_start();
if(isset($_SESSION['admin_session'])){
    echo "<script>window.location='dashboard.php'</script>";
}
require_once("../classes/constants.php");
include("../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
if (isset($_POST['login_submit'])) {

    $user = $sqlQuery->SelectSingle("Select users.*,staff.hotel_id from users left join staff on staff.user_id = users.user_id where users.role_id in(".ADMIN_ROLE_ID.",".STAFF_ROLE_ID.") and users.username='" . $_POST['username'] . "' and users.password='" . md5($_POST['password']) . "'");

    if ($user) {
        $_SESSION['admin_session'] = $user;
        if($_SESSION['admin_session']['role_id']==3){
            echo "<script>window.location='bookings/index.php'</script>";
        }
        echo "<script>window.location='bookings/index.php'</script>";

    } else {
        echo "<script>alert('Invalid username or password.')</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Hotels</title>

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="assets/css/admin.css">

    <link rel="stylesheet" media="screen and (max-width: 1200px)" href="css/admin_resp.css">
    <link rel="stylesheet" type="text/css" href="src/image-uploader.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column" style="background-color: transparent;">

        <!-- Main Content -->
        <div id="content">


            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">


                <div id="register">
                    <form method="post">
                        <div class="login register">

                            <div class="logo">
                                <img src="imgs/logo_admin.png">
                            </div>

                            <h3>Login</h3>

                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control"
                                       placeholder="Password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning" name="login_submit">Login Now</button>
                            </div>
                        </div>
                        <div class="copyright">Copyright Hotels 2020</div>
                    </form>

                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>


        <!-- Bootstrap core JavaScript-->
        <script src="assets/vendor/jquery/jquery.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="assets/js/sb-admin-2.min.js"></script>
        <script src="assets/src/image-uploader.js"></script>
        <script>
            $('.input-images-1').imageUploader();
        </script>