<?php
session_start();
//include "../database/DBConnection.php";
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();

$where_condition = " where true";
$select_query = 'SELECT customers.* , states.name as state_name, cities.name as city_name  FROM customers INNER JOIN states ON states.id = customers.state_id INNER JOIN cities ON cities.id = customers.city_id ';
//$select_query = 'SELECT customers.*, hotels.name as hotel_name , rooms.room_no as room_no FROM bookings LEFT JOIN customers ON bookings.customer_id = customers.customer_id INNER JOIN hotels ON hotels.hotel_id = bookings.hotel_id INNER JOIN rooms on rooms.room_id = bookings.room_id';
//$records = $query->SelectQuery('SELECT bookings.*,customers.*, hotels.name as hotel_name , rooms.room_no as room_no FROM bookings INNER JOIN customers ON bookings.customer_id = customers.customer_id INNER JOIN hotels ON hotels.hotel_id = bookings.hotel_id INNER JOIN rooms on rooms.room_id = bookings.room_id');

if(isset($_POST['fname']) && strval($_POST['fname']) !=''){
    $where_condition.= ' and first_name LIKE "%'. $_POST['fname'].'%"';
}

$data_query = $select_query . $where_condition;

$records = $query->SelectQuery($data_query);
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Customers List</h1>

    </div>

    <div id="listings">
        <div id="searchForm">
            <form method="post" name="customerNameSearch" id="customerNameSearch">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                           <input type="text" placeholder="Customer Name" class="form-control" name="fname" value="<?php echo (isset($_POST['fname'])) ? $_POST['fname'] : '' ;?>">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="customerNameSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Conatct No.</th>
                    <th scope="col">Street Address</th>
                    <th scope="col">State</th>
                    <th scope="col">City</th>
                    <th scope="col">Zipcode</th>

                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $record['first_name'] . " " . $record['last_name'] ?></td>
                            <td><?php echo $record['email_id']  ?></td>
                            <td><?php echo $record['contact_no'] ?></td>
                            <td><?php echo $record['street_address'];  ?></td>
                            <td><?php echo $record['state_name'] ?></td>
                            <td><?php echo $record['city_name'] ?></td>
                            <td><?php echo $record['zip'] ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='8'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("customerNameSearch").reset();
    }
    function resetFormOnclick() {
        document.getElementById("customerNameSearch").reset();
        window.location = 'index.php';
    }

</script>

