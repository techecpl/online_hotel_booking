<?php
session_start();
//include "../database/DBConnection.php";
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();

$where_condition = " where true";
$select_query = 'SELECT bookings.*,customers.*, hotels.name as hotel_name , rooms.room_no as room_no FROM bookings LEFT JOIN customers ON bookings.customer_id = customers.customer_id INNER JOIN hotels ON hotels.hotel_id = bookings.hotel_id INNER JOIN rooms on rooms.room_id = bookings.room_id';
//$records = $query->SelectQuery('SELECT bookings.*,customers.*, hotels.name as hotel_name , rooms.room_no as room_no FROM bookings INNER JOIN customers ON bookings.customer_id = customers.customer_id INNER JOIN hotels ON hotels.hotel_id = bookings.hotel_id INNER JOIN rooms on rooms.room_id = bookings.room_id');

if(isset($_POST['booking_by']) && strval($_POST['booking_by']) !=''){
    $where_condition.= ' and bookings.booking_by = '. $_POST['booking_by'];
}
if(isset($_POST['hotel_id']) && !empty($_POST['hotel_id'])){
    $where_condition.= ' and bookings.hotel_id = '. $_POST['hotel_id'];
}

if(isset($_SESSION['admin_session']['hotel_id']) && !empty($_SESSION['admin_session']['hotel_id'])){
   $where_condition.=  ' and bookings.hotel_id = '. $_SESSION['admin_session']['hotel_id'];
}

$data_query = $select_query . $where_condition;

$records = $query->SelectQuery($data_query);

$hotels = $query->SelectQuery("SELECT * FROM hotels");
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bookings List</h1>

    </div>

    <div id="listings">
        <div id="searchForm">
            <form method="post" name="bookingSearch" id="bookingSearch">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <select name="hotel_id" class="form-control">
                                <option value="0">Select Hotel</option>
                            <?php foreach ($hotels as $hotel){ ?>
                            <option value="<?php echo $hotel['hotel_id']?>" <?php echo (isset($_POST['hotel_id']) && $_POST['hotel_id'] == $hotel['hotel_id']) ? 'selected' : '' ?>><?php echo $hotel['name'] ?> </option>
                            <?php }?>
                            </select>
<!--                            <input type="text" name="room_no" placeholder="Search Hotel Rooms" value="--><?php //echo (isset($_POST['room_no']) && !empty($_POST['room_no'])) ? $_POST['room_no'] : '' ?><!--" class="form-control">-->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <select name="booking_by" class="form-control">
                                <option value="">Select Booking By</option>

                            <option value="0" <?php echo (isset($_POST['booking_by']) && strval($_POST['booking_by']) == strval(0)) ? 'selected' : '' ?>>Customers</option>
                            <option value="1" <?php echo (isset($_POST['booking_by']) && $_POST['booking_by'] == 1) ? 'selected' : '' ?>>Travel Agents</option>

                            </select>
<!--                            <input type="text" name="room_no" placeholder="Search Hotel Rooms" value="--><?php //echo (isset($_POST['room_no']) && !empty($_POST['room_no'])) ? $_POST['room_no'] : '' ?><!--" class="form-control">-->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="bookingSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Hotel Name</th>
                    <th scope="col">Room No</th>
                    <th scope="col">Booking Date</th>
                    <th scope="col">Check In Date</th>
                    <th scope="col">Check out Date</th>
                    <th scope="col">No of Guests</th>
                    <th scope="col">No of Kids</th>
                    <th scope="col">Room Charges</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $record['first_name'] . " " . $record['last_name'] ?></td>
                            <td><?php echo $record['hotel_name']  ?></td>
                            <td><?php echo $record['room_no'] ?></td>
                            <td><?php echo date('d - m - Y',strtotime($record['date_of_booking']));  ?></td>
                            <td><?php echo date('d - m - Y',strtotime($record['check_in'])); ?></td>
                            <td><?php echo date('d - m - Y',strtotime($record['check_out'])); ?></td>
                            <td><?php echo $record['guest'] ?></td>
                            <td><?php echo $record['kids'] ?></td>
                            <td><?php echo $record['room_charges'] ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='7'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("bookingSearch").reset();
    }
    function resetFormOnclick() {
        document.getElementById("bookingSearch").reset();
        window.location = 'index.php';
    }

</script>

