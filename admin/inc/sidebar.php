<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/assignment_online_hotel_booking/constants.php') ;

//echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);exit;
//?>
<style>
    .accordion .active{background-color: #ebb330}
    .sidebar .nav-item .collapse .collapse-inner .collapse-item.active{background-color: #ebb330}
</style>
<!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="hover sidebar-brand d-flex align-items-center justify-content-center" href="index.html">

                <img src="<?php echo ADMIN_ROUTE ?>/imgs/logo_admin.png">
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item ">
                <a class="nav-link hover" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <li class="nav-item">
                <a class="hover nav-link" href="<?php echo ADMIN_ROUTE ?>facilities/index.php">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Facilities</span></a>
            </li>
            <?php
            $active ="";
            $show ="";
            if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_SIDEBAR_ROUTE.'facilities/form-old.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_SIDEBAR_ROUTE.'facilities/list.php')){
                $active = 'active';
                $show = 'show';
                $collapsed = 'collapsed';
            }?>
            <li class="nav-item <?php echo $active; ?>">
                <a class="nav-link hover <?php echo $collapsed; ?> " href="#" data-toggle="collapse" data-target="#facilities"
                   aria-expanded="true" aria-controls="facilities">
                    <i class="fas fa-fw fa-clipboard-list"></i>
                    <span>Facilities</span>
                </a>
                <div id="facilities" class="collapse <?php echo $show; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_SIDEBAR_ROUTE.'facilities/form-old.php'?'active' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>facilities/index.php">Add Facility</a>
                        <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_SIDEBAR_ROUTE.'facilities/list.php'? 'active' :'' ?>" href="<?php echo ADMIN_ROUTE ?>facilities/list.php">Manage Facility</a>
                    </div>
                </div>
            </li>
<?php //print_r(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL));exit();?>
            <?php
            $active ="";
            $show ="";
            if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_SIDEBAR_ROUTE.'room_type/form-old.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_SIDEBAR_ROUTE.'room_type/list.php')){
                $active = 'active';
                $show = 'show';
                $collapsed = 'collapsed';
            }?>
            <li class="nav-item <?php echo $active; ?>">
                <a class="nav-link hover collapsed " href="#" data-toggle="collapse" data-target="#roomType"
                   aria-expanded="true" aria-controls="roomType">
                    <i class="fas fa-fw fa-bed"></i>
                    <span>Room Type</span>
                </a>
                <div id="roomType" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="hover collapse-item" href="<?php echo ADMIN_ROUTE ?>roomType/index.php">Add Room Type</a>
                        <a class="hover collapse-item" href="<?php echo ADMIN_ROUTE ?>roomType/list.php">Manage Room Type</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#hotels"
                    aria-expanded="true" aria-controls="hotels">
                    <i class="fas fa-fw fa-hotel"></i>
                    <span>Hotels</span>
                </a>
                <div id="hotels" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="hover collapse-item" href="<?php echo ADMIN_ROUTE ?>hotels/index.php">Add Hotel</a>
                        <a class="hover collapse-item" href="<?php echo ADMIN_ROUTE ?>hotels/list.php">Manage Hotels</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Menu 1</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="hover collapse-item" href="buttons.html">Buttons</a>
                        <a class="hover collapse-item" href="cards.html">Cards</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Utilities</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Utilities:</h6>
                        <a class="hover collapse-item" href="utilities-color.html">Colors</a>
                        <a class="hover collapse-item" href="utilities-border.html">Borders</a>
                        <a class="hover collapse-item" href="utilities-animation.html">Animations</a>
                        <a class="hover collapse-item" href="utilities-other.html">Other</a>
                    </div>
                </div>
            </li>


            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Pages</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="hover collapse-item" href="#">Login</a>
                        <a class="hover collapse-item" href="#">Register</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="hover nav-link" href="form1.php">
                    <i class="fas fa-fw fa-chart-area"></i>
                    <span>Form</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="hover nav-link" href="tables.html">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Tables</span></a>
            </li>


        </ul>
        <!-- End of Sidebar -->

<script>
    $(window).on('hashchange', function () {
        var ref = location.hash
        var $el = $('a[href="' + ref + '"]')
        var $menu = $el.closest('.hover')

        $('.hover')
            .removeClass('active')
            .find('a')
            .removeClass('active')

        $el.addClass('active')
        $menu.addClass('active')
    });
</script>