<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Hotels</title>

<!-- Custom styles for this template-->
<link href="<?php echo ADMIN_ROUTE?>assets/css/sb-admin-2.min.css" rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="<?php echo ADMIN_ROUTE?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_ROUTE?>assets/css/admin.css">

<link rel="stylesheet" media="screen and (max-width: 1200px)" href="<?php echo ADMIN_ROUTE?>assets/css/admin_resp.css">
<script src="<?php echo ADMIN_ROUTE?>assets/vendor/jquery/jquery.min.js"></script>