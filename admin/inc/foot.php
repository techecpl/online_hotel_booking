<!-- Bootstrap core JavaScript-->
<script src="<?php echo ADMIN_ROUTE?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo ADMIN_ROUTE?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo ADMIN_ROUTE?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo ADMIN_ROUTE?>assets/js/sb-admin-2.min.js"></script>

<script src="<?php echo ADMIN_ROUTE?>src/image-uploader.js"></script>
<script>
    $('.input-images-1').imageUploader();
</script>