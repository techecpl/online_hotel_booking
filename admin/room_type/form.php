<?php
session_start();
require_once("../validate_user.php");
include("../../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
//
//if(isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5  ){
//    unset($_SESSION['s_time']);
//    unset($_SESSION['u_time']);
//}

if (isset($_POST['roomTypeSubmit'])) {
//print_r($_POST);
//exit();
    $_SESSION['s_time'] = time();
    $result = $sqlQuery->InsertQuery("INSERT INTO room_types SET title = '" . $_POST['room_title'] . "', description= '" . $_POST['description'] . "'");
    if (!empty($result)) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record Added Successfully';

    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be added';
        $message = "Record cannot be added";
    }
    header('Location:index.php');
}
if(isset($_POST['roomTypeUpdate'])){
    $id = $_POST['Id'];
    $_SESSION['u_time'] = time();
    $result = $sqlQuery->UpdateQuery('UPDATE room_types SET title ="'.$_POST["room_title"].'",description="'.$_POST["description"].'" WHERE room_type_id="'.$id.'" ');
    if ($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record updated successfully';
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be updated';
    }

    header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
    $result  = array();
    $result ['data'] = $sqlQuery->SelectSingle("SELECT * FROM room_types WHERE room_type_id ='" . $_GET['Id'] . "'");
    $editResult = $result['data'];
}
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Room Type</h1>
    </div>

    <div id="addHotels">
        <form method="post">
            <div class="addHotel_inner">
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="form-group">
                            <label for="room_title">Room Title</label>
                            <input type="text" class="form-control" id="room_title" placeholder=""
                                   name="room_title"
                                   value="<?php echo(isset($editResult['title']) && !empty($editResult['title']) ? $editResult['title'] : '') ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control" required><?php echo(isset($editResult['description']) && !empty($editResult['description']) ? $editResult['description'] : '') ?></textarea>
                        </div>


                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                                echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='roomTypeUpdate' class='btn btn-primary'>Update</button>";
                            } else {
                                echo "<button type='submit' name='roomTypeSubmit' class='btn btn-warning'>Submit</button>";
                            }
                            ?>
                            <button type="submit"  onclick="window.history.go(-1); return false;" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


</div>
<!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>  