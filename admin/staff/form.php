<?php
session_start();
require_once("../validate_user.php");
include("../../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

//if (isset($_SESSION['s_time']) && (time() - $_SESSION['s_time']) > 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time']) > 5) {
//    session_destroy();
//}

$path = ROOM_IMAGE_UPLOAD_PATH;
if (isset($_POST['roomSubmit'])) {
    $_SESSION['s_time'] = time();

    $userId = $sqlQuery->InsertQuery('INSERT INTO users SET role_id=' . STAFF_ROLE_ID . ' ,name="' . $_POST['name'] . '" ,username = "' . $_POST['username'] . '", password = "' . md5($_POST['password']) . '"');


    $query = 'INSERT INTO staff SET name = "' . $_POST["name"]
        . '", email = "' . $_POST["email"]
        . '", user_id= "' . $userId
        . '", hotel_id= "' . $_POST["hotel_id"]
        . '", phone_no = "' . $_POST["phone"]
        . '", address= "' . $_POST["address"]
        . '"';

    $Id = $sqlQuery->InsertQuery($query);

    if (!empty($Id)) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record Added Successfully';

    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be added';
        $message = "Record cannot be added";
    }
    header('Location:index.php');
}
if (isset($_POST['roomUpdate'])) {
    $id = $_POST['Id'];
    $_SESSION['u_time'] = time();

    $query = 'UPDATE staff SET name = "' . $_POST["name"]
        . '", email = "' . $_POST["email"]
        . '", hotel_id= "' . $_POST["hotel_id"]
        . '", phone_no = "' . $_POST["phone"]
        . '", address= "' . $_POST["address"]
        . '"  WHERE staff_id =' . $id;

    $result = $sqlQuery->UpdateQuery($query);

    if ($result == true) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record updated successfully';
    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be updated';
    }

    header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
    $result = array();
    $result ['data'] = $sqlQuery->SelectSingle("SELECT * FROM staff WHERE staff_id ='" . $_GET['Id'] . "'");
    $editResult = $result['data'];
//    print_r($editResult);
//    exit();
}
$hotels = $sqlQuery->SelectQuery("SELECT * FROM hotels");

?>
<?php include('../includes/head.php'); ?>
<style>
    .uploaded {
        width: 100%;
    }

    .uploaded .uploaded-image {
        display: inline-block;
        width: 33%;
        padding: 5px;
    }

    .uploaded img {
        height: 100px;
        width: 100%;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Staff Member</h1>
    </div>

    <div id="addHotels">
        <form method="post" enctype="multipart/form-data">
            <div class="addHotel_inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="state_id">Hotel</label>
                            <select name="hotel_id" id="hotel_id" class="form-control" required>
                                <option value="">Select Hotel</option>
                                <?php foreach ($hotels as $hotel) {
                                    ?>
                                    <option value="<?php echo $hotel['hotel_id'] ?>" <?php echo (isset($editResult['hotel_id']) && !empty($editResult['hotel_id']) && $editResult['hotel_id'] == $hotel['hotel_id']) ? 'selected' : ''; ?>><?php echo $hotel['name'] ?></option>
                                    <?php
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6"
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control"
                               value="<?php echo (isset($editResult['name']) && !empty($editResult['name'])) ? $editResult['name'] : ''; ?>"
                               required>
                    </div>
                </div>
                <div class="row">
                    <?php if (!isset($editResult) || empty($editResult)) { ?>
                        <div class="form-group col-md-6">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control"

                                   required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control"
                                   required>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control"
                               value="<?php echo (isset($editResult['email']) && !empty($editResult['email'])) ? $editResult['email'] : ''; ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="number" name="phone" class="form-control"
                               value="<?php echo (isset($editResult['phone_no']) && !empty($editResult['phone_no'])) ? $editResult['phone_no'] : ''; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea name="address"
                                  class="form-control"><?php echo (isset($editResult['address']) && !empty($editResult['address'])) ? $editResult['address'] : ''; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?php
                        if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                            echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='roomUpdate' class='btn btn-primary'>Update</button>";
                        } else {
                            echo "<button type='submit' name='roomSubmit' class='btn btn-warning'>Submit</button>";
                        }
                        ?>
                        <button type="submit" onclick="window.history.go(-1); return false;"
                                class="btn btn-secondary">Cancel
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>
