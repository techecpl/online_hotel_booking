<?php
session_start();
require_once("../validate_user.php");
//include "../database/DBConnection.php";
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();
//if(isset($_SESSION['d_time']) && (time() - $_SESSION['d_time'] )> 5 || isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5 ){
//    session_destroy();
//}
$where_condition = '';
$select_query = 'SELECT staff.*,hotels.name as hotel_name, users.username as username FROM staff INNER JOIN users on users.user_id = staff.user_id INNER JOIN hotels ON hotels.hotel_id = staff.hotel_id';

$path = ROOM_IMAGE_UPLOAD_PATH;
if (isset($_GET['action']) && $_GET['action'] == 'delete'){
    
    $result = $query->DeleteQuery("DELETE FROM staff WHERE user_id='".$_GET['Id']."' ");
    $result = $query->DeleteQuery("DELETE FROM users WHERE user_id='".$_GET['Id']."' ");
   
    $_SESSION['d_time'] = time();
    if($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record deleted successfully';
        header('Location:index.php');
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be deleted';
        header('Location:index.php');
    }
}
if(isset($_POST['hotel_id'])){
    $where_condition = ' where staff.hotel_id = '. $_POST['hotel_id'];
}
$data_query = $select_query . $where_condition;
$records = $query->SelectQuery($data_query);

$hotels = $query->SelectQuery("SELECT * FROM hotels");
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Staff List</h1>

    </div>

    <div id="listings">

        <div id="searchForm">
            <form method="post" name="roomTypeSearchForm" id="roomTypeSearchForm">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <select name="hotel_id" class="form-control">
                                <option>Select Hotel</option>
                            <?php foreach ($hotels as $hotel){ ?>
                            <option value="<?php echo $hotel['hotel_id']?>" <?php echo (isset($_POST['hotel_id']) && $_POST['hotel_id'] == $hotel['hotel_id']) ? 'selected' : '' ?>><?php echo $hotel['name'] ?> </option>
                            <?php }?>
                            </select>
<!--                            <input type="text" name="room_no" placeholder="Search Hotel Rooms" value="--><?php //echo (isset($_POST['room_no']) && !empty($_POST['room_no'])) ? $_POST['room_no'] : '' ?><!--" class="form-control">-->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="roomTypeSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Hotel</th>
                    <th scope="col">Username</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Address</th>
                    <th scope="col">Action</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $record['hotel_name'] ?></td>
                            <td><?php echo $record['username'] ?></td>
                            <td><?php echo $record['name'] ?></td>
                            <td><?php echo $record['email'] ?></td>
                            <td><?php echo $record['phone_no'] ?></td>
                            <td><?php echo $record['address'] ?></td>
                            <td>
                                <div  class="row">
                                    <div class="col-4 p-1">
                                <a href="<?php echo ADMIN_ROUTE ?>staff/form.php?action=edit&Id=<?php echo $record['staff_id'] ?>" style="margin:0 5px;"><img src="<?php echo ADMIN_ROUTE ?>imgs/edit.jpg" width="22"></a>
                                    </div>
                                    <div class="col-4 p-1">
                                <a href="index.php?action=delete&Id=<?php echo $record['user_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/delete.jpg" width="22"></a>
                                    </div>

                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='7'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("roomTypeSearchForm").reset();
    }
    function resetFormOnclick() {
        document.getElementById("roomTypeSearchForm").reset();
        window.location = 'index.php';
    }

</script>

