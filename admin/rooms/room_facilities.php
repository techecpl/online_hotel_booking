<?php
session_start();
require_once("../validate_user.php");
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();
if (isset($_GET['action']) && ($_GET['action'] == 'facility' || $_GET['action'] == 'editFacility')) {
    $roomId = $_GET['Id'];
}

$roomList = $query->SelectQuery('SELECT room_id,room_no FROM rooms');
$facilityList = $query->SelectQuery('SELECT * FROM facilities WHERE type = 2');

if (isset($_POST['roomFacilitySubmit'])) {
    $_SESSION['s_time'] = time();
    $facilitiesArray = array();
    $facilitiesArray = $_POST['facility_id'];
    if (isset($facilitiesArray) && count($facilitiesArray) > 0) {
        foreach ( $facilitiesArray as $key => $value) {
    $result =  $query->InsertQuery("INSERT INTO room_facilities SET room_id ='".$_POST['room_id']."', facility_id = '".$value."' " );
            if (!empty($result)) {
                $_SESSION['level'] = 'success';
                $_SESSION['message'] = 'Recorded Added Successfully';
            } else {
                $_SESSION['level'] = 'danger';
                $_SESSION['message'] = 'Record cannot be added';
                header('Location:room_facilities.php?action=editFacility&Id='.$roomId);
            }
        }
        header('Location:room_facilities.php?action=editFacility&Id='.$roomId);
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Facility id not found';
        header('Location:room_facilities.php?action=editFacility&Id='.$roomId);
    }
}

if(isset($_POST['roomFacilityUpdate'])){
    $_SESSION['u_time'] = time();
    $facilitiesArray = array();
    $facilitiesArray = $_POST['facility_id'];
    if (isset($facilitiesArray) && count($facilitiesArray) > 0) {
        $query->DeleteQuery("DELETE FROM room_facilities WHERE room_id = '".$roomId."' ");
        foreach ( $facilitiesArray as $key => $value) {
            $result =  $query->InsertQuery("INSERT INTO room_facilities SET room_id = '".$roomId."', facility_id = '".$value."'" );
            if (!empty($result)) {
                $_SESSION['level'] = 'success';
                $_SESSION['message'] = 'Recorded Updated Successfully';
            } else {
                $_SESSION['level'] = 'danger';
                $_SESSION['message'] = 'Record cannot be added';
                header('Location:room_facilities.php?action=editFacility&Id='.$roomId);
            }
        }
        header('Location:room_facilities.php?action=editFacility&Id='.$roomId);
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Facility id not found';
        header('Location:room_facilities.php?action=editFacility&Id='.$roomId);
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'editFacility') {
    $queryResult = $query->SelectQuery("SELECT *,GROUP_CONCAT(facility_id)  as facility_ids FROM room_facilities WHERE room_id='".$roomId."' GROUP BY room_id");
    $editResult = $queryResult[0];
}
?>
<?php include('../includes/head.php'); ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <?php include('../includes/alert.php'); ?>
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <?php if(isset($_GET['action']) && $_GET['action'] == 'editFacility') { ?>
            <h1 class="h3 mb-0 text-gray-800">Edit Room Facilities</h1>
    <?php }else{ ?>
                <h1 class="h3 mb-0 text-gray-800">Add Room Facilities</h1>
            <?php } ?>
        </div>

        <div id="addHotels">
            <form method="post">
                <div class="addHotel_inner">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="form-group">
                                <label for="room_id">Room No</label>
                                <select class="form-control" id="room_id" name="room_id">
                                    <option value="">Select Room</option>
                                    <?php foreach ($roomList as $data) {
                                        $selected = (isset($roomId) && !empty($roomId) && $roomId == $data['room_id']) ? 'selected' : '';
                                        echo "<option value='" . $data['room_id'] . "' " . $selected . ">" . $data['room_no'] . "</option>";
                                    } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="facility_id">Facilities</label>
                                <select class="form-control" id="facility_id" name="facility_id[]" multiple>
                                    <?php
                                    foreach ($facilityList as $data) {
                                        $facilityIds = array();
                                        if(isset($editResult['facility_ids'])){
                                        $facilityIds = explode(',',$editResult['facility_ids']);
                                        }
                                        $selected = (isset($facilityIds) && !empty($facilityIds) && in_array($data['facility_id'],$facilityIds)) ? 'selected' : '';
                                        echo "<option value='" . $data['facility_id'] . "' " . $selected . ">" . $data['name'] . "</option>";
                                    } ?>
                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                if (isset($_GET['action']) && $_GET['action'] == "editFacility" && isset($_GET['Id'])) {
                                    echo " <button type='submit' name='roomFacilityUpdate' class='btn btn-primary'>Update</button>";
                                } else {
                                    echo "<button type='submit' name='roomFacilitySubmit' class='btn btn-warning'>Submit</button>";
                                }
                                ?>
                                <button type="submit" onclick="window.history.go(-1); return false;"
                                        class="btn btn-secondary">Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>


    </div>
    <!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>