<?php
session_start();
require_once("../validate_user.php");
include("../../classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

//if (isset($_SESSION['s_time']) && (time() - $_SESSION['s_time']) > 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time']) > 5) {
//    session_destroy();
//}

$path = ROOM_IMAGE_UPLOAD_PATH;
if (isset($_POST['roomSubmit'])) {
//    print_r($_POST);
//    exit();
    $_SESSION['s_time'] = time();
    
    $query = 'INSERT INTO rooms SET room_no = "' . $_POST["room_no"]
            . '", no_of_beds = "' . $_POST["no_of_beds"] 
            . '", room_type_id= "' . $_POST["room_type_id"] 
            . '", hotel_id= "' . $_POST["hotel_id"] 
            . '", room_charges= "' . $_POST["room_charges"] 
            . '", description= "' . $_POST["description"] 
            . '"';
    
    $Id = $sqlQuery->InsertQuery($query);

    if (isset($_FILES['images']['name']) && !empty($_FILES['images']['name'][0])) {
        $response = $sqlQuery->UpdateQuery("UPDATE rooms SET image ='" . $Id . '-' . $_FILES['images']['name'][0] . "' WHERE room_id='" . $Id . "' ");

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $target_file = $path . $Id . '-' . basename($_FILES['images']['name'][0]);
        if (move_uploaded_file($_FILES["images"]["tmp_name"][0], $target_file)) {
            echo "The file " . htmlspecialchars(basename($_FILES['images']['name'][0])) . " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    if (!empty($Id)) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Recorded Added Successfully';

    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be added';
        $message = "Record cannot be added";
    }
    header('Location:index.php');
}
if (isset($_POST['roomUpdate'])) {
    $id = $_POST['Id'];
    $_SESSION['u_time'] = time();
    
     $query = 'UPDATE rooms SET room_no = "' . $_POST["room_no"]
            . '", no_of_beds = "' . $_POST["no_of_beds"] 
            . '", room_type_id= "' . $_POST["room_type_id"] 
            . '", hotel_id= "' . $_POST["hotel_id"] 
            . '", room_charges= "' . $_POST["room_charges"] 
            . '", description= "' . $_POST["description"] 
            . '"  WHERE room_id='.$id;
     
    $result = $sqlQuery->UpdateQuery($query);

    if (isset($_FILES['images']['name']) && !empty($_FILES['images']['name'][0])) {
        $checkResult = $sqlQuery->SelectSingle("SELECT * FROM rooms WHERE room_id = '" . $id . "'");
        $filename = $path . $checkResult['image'];
        if (file_exists($filename)) {
            unlink($filename);
            $output = array('status' => 'success');
        } else {
            $output = array('status' => 'fail');
        }
        $imageResult = $sqlQuery->UpdateQuery("UPDATE rooms SET image ='" . $id . '-' . $_FILES['images']['name'][0] . "' WHERE room_id='" . $id . "' ");
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $target_file = $path . $id . '-' . basename($_FILES['images']['name'][0]);

        if (move_uploaded_file($_FILES["images"]["tmp_name"][0], $target_file)) {
            echo "The file " . htmlspecialchars(basename($_FILES['images']['name'][0])) . " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    if ($result == true) {
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record updated successfully';
    } else {
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be updated';
    }

    header('Location:index.php');
}

if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
    $result = array();
    $result ['data'] = $sqlQuery->SelectSingle("SELECT * FROM rooms WHERE room_id ='" . $_GET['Id'] . "'");
    $editResult = $result['data'];
//    print_r($editResult);
//    exit();
}

$roomTypes = $sqlQuery->SelectQuery("SELECT * FROM room_types");
$hotels = $sqlQuery->SelectQuery("SELECT * FROM hotels");

?>
<?php include('../includes/head.php'); ?>
<style>
    .uploaded {
        width: 100%;
    }

    .uploaded .uploaded-image {
        display: inline-block;
        width: 33%;
        padding: 5px;
    }

    .uploaded img {
        height: 100px;
        width: 100%;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add Room</h1>
    </div>

    <div id="addHotels">
        <form method="post" enctype="multipart/form-data">
            <div class="addHotel_inner">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="state_id">Hotel</label>
                            <select name="hotel_id" id="hotel_id" class="form-control" required>
                                <option value="">Select Hotel</option>
                                <?php foreach ($hotels as $hotel) {
                                    ?>
                                    <option value="<?php echo $hotel['hotel_id'] ?>" <?php echo (isset($editResult['hotel_id']) && !empty($editResult['hotel_id']) && $editResult['hotel_id'] == $hotel['hotel_id']) ? 'selected' : ''; ?>><?php echo $hotel['name'] ?></option>
                                    <?php
                                } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="room_type_id">Room Type</label>
                            <select name="room_type_id" id="room_type_id" class="form-control" required>
                                <option value="">Select Room Type</option>
                                <?php foreach ($roomTypes as $type) {
                                    ?>
                                    <option value="<?php echo $type['room_type_id'] ?>" <?php echo (isset($editResult['room_type_id']) && !empty($editResult['room_type_id']) && $editResult['room_type_id'] == $type['room_type_id']) ? 'selected' : ''; ?>><?php echo $type['title'] ?></option>
                                    <?php
                                } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="uploadimage">Upload Image</label>
                            <div class="input-images-1"></div>
                            <?php if (isset($editResult['image']) && !empty($editResult['image'])) { ?>
                                <div class="uploaded">
                                    <div class="uploaded-image" data-index="1"><img
                                                src="<?php echo FRONTEND_UPLOAD_PATH_ORG.'room_images/' . $editResult['image']; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="room_no">Room No.</label>
                            <input type="text" name="room_no" class="form-control"
                                   value="<?php echo (isset($editResult['room_no']) && !empty($editResult['room_no'])) ? $editResult['room_no'] : ''; ?>"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="no_of_beds">No. of beds</label>
                            <input type="number" name="no_of_beds" class="form-control"
                                   value="<?php echo (isset($editResult['no_of_beds']) && !empty($editResult['no_of_beds'])) ? $editResult['no_of_beds'] : ''; ?>"
                                   required>
                        </div>
                         <div class="form-group">
                            <label for="room_charges">Room Charges</label>
                            <input type="number" name="room_charges" class="form-control"
                                   value="<?php echo (isset($editResult['room_charges']) && !empty($editResult['room_charges'])) ? $editResult['room_charges'] : ''; ?>"
                                   required>
                        </div>

                    </div>
                </div>
                 <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control"><?php echo (isset($editResult['description']) && !empty($editResult['description'])) ? $editResult['description'] : ''; ?></textarea>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            if (isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['Id'])) {
                                echo "<input type='hidden' value='" . $_GET['Id'] . "' name='Id'>
                                            <button type='submit' name='roomUpdate' class='btn btn-primary'>Update</button>";
                            } else {
                                echo "<button type='submit' name='roomSubmit' class='btn btn-warning'>Submit</button>";
                            }
                            ?>
                            <button type="submit" onclick="window.history.go(-1); return false;"
                                    class="btn btn-secondary">Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


</div>
<!-- /.container-fluid -->

<?php include('../includes/footer.php'); ?>
