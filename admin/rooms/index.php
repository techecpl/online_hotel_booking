<?php
session_start();
require_once("../validate_user.php");
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();

$records = $query->SelectQuery('SELECT rooms.*,hotels.name as hotel_name, room_types.title as room_type_name FROM rooms INNER JOIN hotels ON hotels.hotel_id = rooms.hotel_id INNER JOIN room_types ON room_types.room_type_id = rooms.room_type_id ORDER BY rooms.room_type_id ');
$path = ROOM_IMAGE_UPLOAD_PATH;
if (isset($_GET['action']) && $_GET['action'] == 'delete'){
    $checkResult = $query->SelectSingle("SELECT * FROM rooms WHERE room_id = '".$_GET['Id']."'");
    $filename = $path.$checkResult['image'];
    $result = $query->DeleteQuery("DELETE FROM rooms WHERE room_id='".$_GET['Id']."' ");
    if (file_exists($filename)) {
        unlink($filename);
        $response = array('status' => 'success');
    } else {
        $response = array('status' => 'fail');
    }
    $_SESSION['d_time'] = time();
    if($result == true){
        $_SESSION['level'] = 'success';
        $_SESSION['message'] = 'Record deleted successfully';
        header('Location:index.php');
    }else{
        $_SESSION['level'] = 'danger';
        $_SESSION['message'] = 'Record cannot be deleted';
        header('Location:index.php');
    }
}
if(isset($_POST['hotel_id'])){

    $result = $query->SelectQuery("SELECT rooms.*,hotels.name as hotel_name, room_types.title as room_type_name FROM rooms INNER JOIN hotels ON hotels.hotel_id = rooms.hotel_id INNER JOIN room_types ON room_types.room_type_id = rooms.room_type_id WHERE rooms.hotel_id LIKE '%".$_POST['hotel_id']."%' ");
    $records = $result;
}
$hotels = $query->SelectQuery("SELECT * FROM hotels");
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Rooms List</h1>

    </div>

    <div id="listings">

        <div id="searchForm">
            <form method="post" name="roomTypeSearchForm" id="roomTypeSearchForm">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <select name="hotel_id" class="form-control">
                                <option>Select Hotel</option>
                            <?php foreach ($hotels as $hotel){ ?>
                            <option value="<?php echo $hotel['hotel_id']?>" <?php echo (isset($_POST['hotel_id']) && $_POST['hotel_id'] == $hotel['hotel_id']) ? 'selected' : '' ?>><?php echo $hotel['name'] ?> </option>
                            <?php }?>
                            </select>
<!--                            <input type="text" name="room_no" placeholder="Search Hotel Rooms" value="--><?php //echo (isset($_POST['room_no']) && !empty($_POST['room_no'])) ? $_POST['room_no'] : '' ?><!--" class="form-control">-->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="roomTypeSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Image</th>
                    <th scope="col">Room No</th>
                    <th scope="col">Type</th>
                    <th scope="col">No Of beds</th>
                    <th scope="col">Hotel</th>
                    <th scope="col">Action</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                       $count =  $query->CountQuery("SELECT * FROM room_facilities WHERE room_id ='".$record['room_id']."'");
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><img src="<?php echo FRONTEND_UPLOAD_PATH_ORG.'room_images/'.$record['image'] ?>" width="50px" height="50px"/></td>
                            <td><?php echo $record['room_no'] ?></td>
                            <td><?php echo $record['room_type_name'] ?></td>
                            <td><?php echo $record['no_of_beds'] ?></td>
                            <td><?php echo $record['hotel_name'] ?></td>
                            <td>
                                <div  class="row">
                                    <div class="col-4 p-1">
                                <a href="<?php echo ADMIN_ROUTE ?>rooms/form.php?action=edit&Id=<?php echo $record['room_id'] ?>" style="margin:0 5px;"><img src="<?php echo ADMIN_ROUTE ?>imgs/edit.jpg" width="22"></a>
                                    </div>
                                    <div class="col-4 p-1">
                                <a href="index.php?action=delete&Id=<?php echo $record['room_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/delete.jpg" width="22"></a>
                                    </div>
                                    <?php if($count == 0){?>
                                        <div class="col-4 p-1">
                                            <a href="<?php echo ADMIN_ROUTE ?>rooms/room_facilities.php?action=facility&Id=<?php echo $record['room_id'] ?>"><i class="text-warning fas fa-plus-square fa-2x"></i> </a>
                                        </div>
                                    <?php } else{?>
                                        <div class="col-4 p-1">
                                            <a href="<?php echo ADMIN_ROUTE ?>rooms/room_facilities.php?action=editFacility&Id=<?php echo $record['room_id'] ?>"><img src="<?php echo ADMIN_ROUTE ?>imgs/paper-edit.png" width="22"> </a>
                                        </div>
                                    <?php }?>

                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='7'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("roomTypeSearchForm").reset();
    }
    function resetFormOnclick() {
        document.getElementById("roomTypeSearchForm").reset();
        window.location = 'index.php';
    }

</script>

