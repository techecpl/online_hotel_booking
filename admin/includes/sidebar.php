<style>
    .accordion .active{background-color: #ebb330}
    .sidebar .nav-item .collapse .collapse-inner .collapse-item.active{background-color: #ebb330}
</style>
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="hover sidebar-brand d-flex align-items-center justify-content-center" href="index.html">

        <img src="<?php echo ADMIN_ROUTE ?>imgs/logo_admin.png">
    </a>
    <?php
    if($_SESSION['admin_session']['role_id']==3){
      ?>
        <hr class="sidebar-divider my-0">
        <li class="nav-item ">
            <a class="nav-link hover" href="<?php echo ADMIN_ROUTE ?>bookings/index.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Booking Records</span></a>
        </li>
        <hr class="sidebar-divider">
    <?php
    }else{
    ?>


    <!-- Divider -->
    <hr class="sidebar-divider my-0">


    <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'facilities/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'facilities/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover <?php echo $collapsed; ?> " href="#" data-toggle="collapse" data-target="#facilities"
           aria-expanded="true" aria-controls="facilities">
            <i class="fas fa-fw fa-clipboard-list"></i>
            <span>Facilities</span>
        </a>
        <div id="facilities" class="collapse <?php echo $show; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'facilities/form.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>facilities/form.php">Add Facility</a>
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'facilities/index.php'? 'active text-white' :'' ?>" href="<?php echo ADMIN_ROUTE ?>facilities/index.php">Manage Facility</a>
            </div>
        </div>
    </li>
    <?php //print_r(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL));exit();?>
    <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'room_type/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'room_type/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover collapsed " href="#" data-toggle="collapse" data-target="#roomType"
           aria-expanded="true" aria-controls="roomType">
            <i class="fas fa-fw fa-bed"></i>
            <span>Room Type</span>
        </a>
        <div id="roomType" class="collapse <?php echo $show;?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'room_type/form.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>room_type/form.php">Add Room Type</a>
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'room_type/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>room_type/index.php">Manage Room Type</a>
            </div>
        </div>
    </li>
    <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'hotels/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'hotels/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#hotels"
           aria-expanded="true" aria-controls="hotels">
            <i class="fas fa-fw fa-hotel"></i>
            <span>Hotels</span>
        </a>
        <div id="hotels" class="collapse <?php echo $show;?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'hotels/form.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>hotels/form.php">Add Hotel</a>
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'hotels/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>hotels/index.php">Manage Hotels</a>
            </div>
        </div>
    </li>
    <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'rooms/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'rooms/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#rooms"
           aria-expanded="true" aria-controls="rooms">
            <i class="fas fa-fw fa-bed"></i>
            <span>Rooms</span>
        </a>
        <div id="rooms" class="collapse <?php echo $show;?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'rooms/form.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>rooms/form.php">Add Room</a>
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'rooms/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>rooms/index.php">Manage Rooms</a>
            </div>
        </div>
    </li>
    <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'bookings/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'bookings/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
           aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Bookings</span>
        </a>
        <div id="collapseTwo" class="collapse <?php echo $show;?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
               <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'bookings/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>bookings/index.php">Booking Records</a>
            </div>
        </div>
    </li>
    
    
     <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'staff/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'staff/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#staffs"
           aria-expanded="true" aria-controls="staff">
            <i class="fas fa-fw fa-user"></i>
            <span>Hotel Staff</span>
        </a>
        <div id="staffs" class="collapse <?php echo $show;?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'staff/form.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>staff/form.php">Add Staff</a>
                <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'staff/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>staff/index.php">Manage Staff</a>
            </div>
        </div>
    </li>
        <?php
        $active ="";
        $show ="";
        if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'customers/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'customers/index.php')){
            $active = 'active';
            $show = 'show';
            $collapsed = 'collapsed';
        }?>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item <?php echo $active; ?>">
            <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#customers"
               aria-expanded="true" aria-controls="customers">
                <i class="fas fa-fw fa-user"></i>
                <span>Customers</span>
            </a>
            <div id="customers" class="collapse <?php echo $show;?>" aria-labelledby="headingThree" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'customers/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>customers/index.php">Customers Record</a>
                </div>
            </div>
        </li>
    <?php
    $active ="";
    $show ="";
    if((filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'travel_agents/form.php' || filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)== ADMIN_ROUTE.'travel_agents/index.php')){
        $active = 'active';
        $show = 'show';
        $collapsed = 'collapsed';
    }?>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php echo $active; ?>">
        <a class="nav-link hover collapsed" href="#" data-toggle="collapse" data-target="#collapseSeven"
           aria-expanded="true" aria-controls="collapseSeven">
            <i class="fas fa-fw fa-users"></i>
            <span>Travel Agents</span>
        </a>
        <div id="collapseSeven" class="collapse <?php echo $show;?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
               <a class="hover collapse-item <?php echo filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL)  == ADMIN_ROUTE.'travel_agents/index.php'?'active text-white' : ''; ?>" href="<?php echo ADMIN_ROUTE ?>travel_agents/index.php">Travel Agents</a>
            </div>
        </div>
    </li>
    <?php
    }
    ?>

</ul>
<!-- End of Sidebar -->

<!--<script>-->
<!--    $(window).on('hashchange', function () {-->
<!--        var ref = location.hash-->
<!--        var $el = $('a[href="' + ref + '"]')-->
<!--        var $menu = $el.closest('.hover')-->
<!---->
<!--        $('.hover')-->
<!--            .removeClass('active')-->
<!--            .find('a')-->
<!--            .removeClass('active')-->
<!---->
<!--        $el.addClass('active')-->
<!--        $menu.addClass('active')-->
<!--    });-->
<!--</script>-->