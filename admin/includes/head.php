<?php
if(isset($_SESSION['d_time']) && (time() - $_SESSION['d_time'] ) > 5 ||isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5  ){
    unset($_SESSION['s_time'], $_SESSION['level'] ,$_SESSION['message']);
    unset($_SESSION['u_time']);
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Hotels</title>

        <!-- Custom styles for this template-->
        <link href="<?php echo ADMIN_ROUTE?>assets/css/sb-admin-2.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="<?php echo ADMIN_ROUTE?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo ADMIN_ROUTE?>assets/css/admin.css">

        <link rel="stylesheet" media="screen and (max-width: 1200px)" href="<?php echo ADMIN_ROUTE?>assets/css/admin_resp.css">
        <link rel="stylesheet" type="text/css" href="<?php echo ADMIN_ROUTE?>assets/src/image-uploader.css">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">



    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php include('sidebar.php'); ?>
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">
                    <?php include('header.php'); ?>