<?php
session_start();
require_once("../validate_user.php");
//include "../database/DBConnection.php";
require_once('../../classes/SqlQueries.php');
$query = new SqlQueries();
if(isset($_SESSION['d_time']) && (time() - $_SESSION['d_time'] )> 5 || isset($_SESSION['s_time']) && (time() - $_SESSION['s_time'] )> 5 || isset($_SESSION['u_time']) && (time() - $_SESSION['u_time'] )> 5 ){
    session_destroy();
}
$where_condition = ' ';
$select_query = 'SELECT travel_agents.*,states.name as state_name, cities.name as city_name FROM travel_agents INNER JOIN states on states.id = travel_agents.state_id INNER JOIN cities ON cities.id = travel_agents.city_id';

if(isset($_POST['company_name'])){
    $where_condition .= ' where travel_agents.company_name = "'. $_POST['company_name'].'"';
}

$data_query = $select_query . $where_condition;


$records = $query->SelectQuery($data_query);

//$hotels = $query->SelectQuery("SELECT * FROM hotels");
?>
<?php include('../includes/head.php'); ?>
<!-- Begin Page Content -->

<div class="container-fluid">
    <?php include('../includes/alert.php'); ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Travel Agent List</h1>

    </div>

    <div id="listings">

        <div id="searchForm">
           <form method="post" name="roomTypeSearchForm" id="roomTypeSearchForm">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <input type="text" name="company_name" placeholder="Company Name" value="<?php echo (isset($_POST['company_name']) && !empty($_POST['company_name'])) ? $_POST['company_name'] : '' ?>" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="form-group">
                            <button class="btn btn-warning" name="roomTypeSearch" type="submit">Search</button>
                            <button class="btn btn-secondary" type="reset" onclick="resetFormOnclick()">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th scope="col">Sr. No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Company Name</th>
                    <th scope="col">Company Address</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                </tr>
                <?php
                $no = 1;
                if (isset($records) && count($records) > 0) {
                    foreach ($records as $record) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $record['first_name'] . " ". $record["last_name"] ?></td>
                            <td><?php echo $record['company_name'] ?></td>
                            <td><?php echo $record['company_address'] ?></td>
                            <td><?php echo $record['email_id'] ?></td>
                            <td><?php echo $record['contact_no'] ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    echo "    <tr>
                                    <td colspan='6'>No records found</td>
                                </tr>";
                }
                ?>
            </table>
        </div>
    </div>


</div>

<!-- /.container-fluid -->
<?php include('../includes/footer.php'); ?>  
<script>
    $(document).ready(function () {
        resetForms();
    });

    function resetForms() {
        document.getElementById("roomTypeSearchForm").reset();
    }
    function resetFormOnclick() {
        document.getElementById("roomTypeSearchForm").reset();
        window.location = 'index.php';
    }

</script>

