<?php
session_start();
//unset($_SESSION['pay']);
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
//print_r($_SESSION['user_session']);
//exit();
if (isset($_SESSION['user_session'])) {
    redirect('index.php');
}
$role_id = CUSTOMER_ROLE_ID;
$more_fields = "";
$table = "";
$state_id = '';
$city_id = '';
if (isset($_POST['register_details_submit'])) {
    $table = 'customers';
    $state_id = $_POST['user_state_id'];
    $city_id = $_POST['user_city_id'];
    $role_id = CUSTOMER_ROLE_ID;
    $more_fields = ", street_address = '" . $_POST['address'] . "' ";
} elseif (isset($_POST['agent_register_details_submit'])) {
    $table = 'travel_agents';
    $state_id = $_POST['agent_state_id'];
    $city_id = $_POST['agent_city_id'];
    $role_id = TRAVEL_AGENT_ROLE_ID;
    $more_fields = ", company_address = '" . $_POST['company_address'] . "' , company_name ='" . $_POST['company_name'] . "' ";
}

if (isset($_POST['register_details_submit']) || isset($_POST['agent_register_details_submit'])) {

    $UserId = $sqlQuery->InsertQuery('INSERT INTO users SET role_id="' . $role_id . '" ,name="' . $_POST['fname'] . '" ,username = "' . $_POST['username'] . '", password = "' . md5($_POST['password']) . '"');

    $filepath = UPLOAD_PATH_ORG . 'userid_proofs/';
    $filename = $UserId . '-' . $_FILES['id_proof']['name'];

    if (!file_exists($filepath)) {
        mkdir($filepath, 0777, true);
    }
    $target_file = $filepath . $filename;

    move_uploaded_file($_FILES['id_proof']['tmp_name'], $target_file);


    $customer_user_Id = $sqlQuery->InsertQuery("INSERT INTO 
" . $table . " SET user_id = '" . $UserId . "', first_name =  '" . $_POST['fname'] . "', last_name = '" . $_POST['lname'] . "',  city_id = '" . $city_id . "', state_id = '" . $state_id . "', zip = '" . $_POST['zipcode'] . "', contact_no = '" . $_POST['contact_no'] . "', email_id = '" . $_POST['emailid'] . "', id_proof ='" . $filename . "' {$more_fields }");

    echo "<script>alert('Account created successfully. Please login to your account to proceed further.')</script>";
    redirect('login_register.php');
}


if (isset($_POST['login_submit'])) {
    $user = $sqlQuery->SelectSingle("Select users.*,customers.customer_id, customers.first_name,travel_agents.travel_agent_id, travel_agents.first_name from users left join customers on customers.user_id = users.user_id left join travel_agents on travel_agents.user_id = users.user_id where users.username='" . $_POST['username'] . "' and users.password='" . md5($_POST['password']) . "'");
    if ($user) {
        $_SESSION['user_session'] = $user;
        echo "<script>alert('Login Successfully.')</script>";
    } else {
        echo "<script>alert('Invalid username or password.')</script>";
        redirect('login_register.php');
    }

    if (isset($_SESSION['roombooking_cart'])) {
        if ($_SESSION['user_session']['role_id'] == TRAVEL_AGENT_ROLE_ID) {
            redirect('customer_details.php');
        } else {
            redirect('booking.php');
        }
    } else {
        redirect('index.php');
    }
}
?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
    <h1>Personal Details</h1>
</section>
<style>
    .uploaded {
        width: 100%;
    }

    .uploaded .uploaded-image {
        display: inline-block;
        width: 33%;
        padding: 5px;
    }

    .uploaded img {
        height: 100px;
        width: 100%;
    }

    .btn-light {
        color: #212529;
        background-color: #ffffff;
        border-color: #c6c6c6;
    }
</style>

<?php include('inc/search.php'); ?>
<section id="listings">
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation"><a class="nav-link active" id="login-tab" data-toggle="tab"
                                                        href="#booking_login" role="tab" aria-controls="booking_login"
                                                        aria-selected="true">Login</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" id="register-tab" data-toggle="tab"
                                                        href="#booking_register" role="tab"
                                                        aria-controls="booking_register"
                                                        aria-selected="false">Register</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" id="register-agent-tab" data-toggle="tab"
                                                        href="#booking_register_travelagent" role="tab"
                                                        aria-controls="booking_register_travelagent"
                                                        aria-selected="false">Register as travel agent</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="booking_login" role="tabpanel" aria-labelledby="login-tab">
                <div class="log-regis-form">
                    <h3>Login</h3>
                    <form method="post">
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="login_submit" class="btn btn-login">Login</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="booking_register" role="tabpanel" aria-labelledby="register-tab">
                <div class="log-regis-form">
                    <h3>Register</h3>
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" name="fname" class="form-control" placeholder="First Name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="emailid" class="form-control" placeholder="Email ID" required>
                        </div>

                        <div class="form-group">
                            <input type="text" name="contact_no" class="form-control" placeholder="Contact No" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control selectpicker" data-live-search="true" name="user_state_id"
                                    id="user_state_id" required>
                                <option value="">Select County</option>
                                <?php foreach ($county as $data) { ?>
                                    <option value="<?php echo $data['id'] ?>" <?php echo (isset($_POST['state_id']) && !empty($_POST['state_id']) && $_POST['state_id'] == $data['id']) ? 'selected' : '' ?>><?php echo $data['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control selectpicker" data-live-search="true" name="user_city_id"
                                    id="user_city_id" required>
                                <option value="">Select City</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="zipcode" class="form-control" placeholder="Zipcode" required>
                        </div>
                        <div class="form-group">
                            <textarea name="address" class="form-control" placeholder="Address" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="uploadimage">Id Proof</label>
                            <input type="file" name="id_proof" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="register_details_submit" class="btn btn-login">Register</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="booking_register_travelagent" role="tabpanel"
                 aria-labelledby="register-agent-tab">
                <div class="log-regis-form">
                    <h3>Register</h3>
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" name="fname" class="form-control" placeholder="First Name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="emailid" class="form-control" placeholder="Email ID" required>
                        </div>

                        <div class="form-group">
                            <input type="text" name="contact_no" class="form-control" placeholder="Contact No" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control selectpicker" data-live-search="true" name="agent_state_id"
                                    id="agent_state_id" required>
                                <option value="">Select County</option>
                                <?php foreach ($county as $data) { ?>
                                    <option value="<?php echo $data['id'] ?>" <?php echo (isset($_POST['state_id']) && !empty($_POST['state_id']) && $_POST['state_id'] == $data['id']) ? 'selected' : '' ?>><?php echo $data['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control selectpicker" data-live-search="true" name="agent_city_id"
                                    id="agent_city_id" required>
                                <option value="">Select City</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="zipcode" class="form-control" placeholder="Zipcode" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="company_name" class="form-control" placeholder="Company Name"
                                   required>
                        </div>
                        <div class="form-group">
                            <textarea name="company_address" class="form-control" placeholder="Company Address"
                                      required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="uploadimage">Id Proof</label>
                            <input type="file" name="id_proof" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="agent_register_details_submit" class="btn btn-login">Register
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

<?php include('inc/footer.php'); ?>
<script>
    var state_id = $('#user_state_id option:selected').val();
    if (state_id) {
        stateCityDropdown(state_id);
    }
    $('#user_state_id').on("change", function () {
        var state_id = $('#user_state_id option:selected').val();
        stateCityDropdown(state_id);
    });

    function stateCityDropdown(state) {
        $(".selectpicker#user_city_id").html('');
        $.ajax({
            url: "<?php echo FRONTEND_ROUTE . 'get_cities.php'; ?>",
            type: "POST",
            data: {state: state},
            dataType: "json",
            success: function (data) {
                if (data) {
                    var selectedCityId = "<?php echo (isset($_POST['city_id']) && !empty($_POST['city_id'])) ? $_POST['city_id'] : '' ?>";
                    var options = [];
                    // $("#user_city_id").append("<option value=''>Select City</option>");
                    $('.selectpicker#user_city_id').html("<option value=''>Select City</option>");
                    $.map(data, function (val, i) {

                        var city_name = val.name;
                        var selected = (selectedCityId == val.id) ? "selected=selected" : "";
                        var option = "<option  " + selected + "   value='" + val.id + "' data-id='" + val.id + "'>" + city_name + "</option>";
                        options.push(option);
                        // $("#user_city_id").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                    $('.selectpicker#user_city_id').append(options);
                    $('.selectpicker#user_city_id').selectpicker('refresh');
                }

            }
        });
    }

    var state_id = $('#agent_state_id option:selected').val();
    if (state_id) {
        agentStateCityDropdown(state_id);
    }
    $('#agent_state_id').on("change", function () {
        var state_id = $('#agent_state_id option:selected').val();
        agentStateCityDropdown(state_id);
    });

    function agentStateCityDropdown(state) {
        $(".selectpicker#agent_city_id").html('');
        $.ajax({
            url: "<?php echo FRONTEND_ROUTE . 'get_cities.php'; ?>",
            type: "POST",
            data: {state: state},
            dataType: "json",
            success: function (data) {
                if (data) {
                    var selectedCityId = "<?php echo (isset($_POST['city_id']) && !empty($_POST['city_id'])) ? $_POST['city_id'] : '' ?>";
                    var options = [];
                    // $("#agent_city_id").append("<option value=''>Select City</option>");
                    $('.selectpicker#agent_city_id').html("<option value=''>Select City</option>");
                    $.map(data, function (val, i) {

                        var city_name = val.name;
                        var selected = (selectedCityId == val.id) ? "selected=selected" : "";
                        var option = "<option  " + selected + "   value='" + val.id + "' data-id='" + val.id + "'>" + city_name + "</option>";
                        options.push(option);
                        // $("#agent_city_id").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                    $('.selectpicker#agent_city_id').append(options);
                    $('.selectpicker#agent_city_id').selectpicker('refresh');
                }

            }
        });
    }
</script>
