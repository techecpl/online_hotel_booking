<?php
session_start();
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
$hotels = $sqlQuery->SelectQuery("SELECT hotels.*,states.name as state_name, cities.name as city_name FROM hotels INNER JOIN states ON states.id = hotels.state_id INNER JOIN cities ON cities.id = hotels.city_id LIMIT 5");

foreach ($hotels as $key => $record) {
    $hotels[$key]['hotel_facilities'] = $sqlQuery->SelectQuery('SELECT hotel_facilities.*,facilities.name,facilities.icon FROM hotel_facilities INNER JOIN facilities ON facilities.facility_id = hotel_facilities.facility_id  WHERE hotel_id ="' . $record['hotel_id'] . '" ');
}
//print_r($hotels);
//exit();

$facilities = $sqlQuery->SelectQuery("SELECT * FROM facilities WHERE type=1");
$county = $sqlQuery->SelectQuery("SELECT * FROM states ORDER BY name ASC");
?>
<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active" style="background-image: url('banners/b1.jpg')"></div>
            <div class="carousel-item" style="background-image: url('banners/b2.jpg')"></div>
            <div class="carousel-item" style="background-image: url('banners/b3.jpg')"></div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<section id="checkin_form">
    <form method="post" action="hotels.php">
    <div class="form-inner">
        <h3>Book Rooms</h3>
        <div class="form-group">
            <select class="form-control selectpicker" data-live-search="true" name="state_id" id="state_id">
                <option value="">Select County</option>
                <?php foreach ($county as $data){ ?>
                    <option value="<?php echo $data['id']?>" ><?php echo $data['name']?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
           <select class="form-control selectpicker" data-live-search="true" name="city_id" id="city_id">
               <option value="">Select City</option>
           </select>
        </div>
        <div class="form-group">
            <input type="text" name="checkin" class="form-control fromDate" placeholder="Check-in"  autocomplete="off" required>
        </div>
        <div class="form-group">
            <input type="text" name="checkout" class="form-control toDate" placeholder="Check-out"  autocomplete="off" required>
        </div>
        <div class="form-group">
            <select name="guest" class="form-control">
                <option value="">No of Guests</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        <div class="form-group">
            <select name="kids" class="form-control">
                <option value="">No of Kids</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        <div class="form-group text-center">
            <button type="submit" name="submitHotelSearchDetails" class="btn btn-book">Check Avaiability</button>
        </div>
    </div>
    </form>
</section>


<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="imgs/about.png" class="img-fluid">
            </div>
            <div class="col-md-7">
                <div class="about_box">
                    <h1>Welcome to Hilton Hotels</h1>
                    <p class="sm-head">High quality accommodation services</p>
                    <p style="text-justify">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

                    <p style="text-justify">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="listings" class="light-grey">
    <div class="container">
        <h2>List of Hotels</h2>
        <?php foreach ($hotels as $hotel){?>
        <div class="inner_box">
            <div class="row">
                <div class="col-md-12 col-lg-4">
                    <div class="img-box">
                        <div class="img-hover-zoom"><img src="<?php echo  FRONTEND_UPLOAD_PATH_ORG.'hotel_images/'.$hotel['image']?>" class="img-fluid"></div>
                        <div class="starrat">
                            <?php echo $hotel['type']?> <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-8">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <h2><?php echo $hotel['name'] ?></h2>
                                <p><?php echo $hotel['description']?></p>

                                <ul class="special_feat">
                                    <?php foreach ($hotel['hotel_facilities'] as $h_facilities) { ?>
                                        <li><i class="<?php echo $h_facilities['icon'] ?>"></i> <?php echo $h_facilities['name'] ?></li>

                                    <?php } ?>

                                </ul>

                                <div class="price_box">
                                    <a href="rooms.php?hotel_id=<?php echo $hotel['hotel_id']; ?>"
                                       class="btn btn-details">Book Rooms</a>
                                </div>

                            </div>
<!--                            <div class="col-md-3">-->
<!--                                <div class="price_box">-->
<!--                                    <h3><i class="fas fa-rupee-sign"></i> 3250/-</h3>-->
<!--                                    <p class="sm-head">per night</p>-->
<!--                                    <a href="rooms.php?hotel_id=--><?php //echo $hotel['hotel_id']; ?><!--"-->
<!--                                       class="btn btn-details">Book Rooms</a>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>


<section id="facility_hm">
    <div class="opacity"></div>
    <div class="container">
        <div class="inner_box">
            <h2>Our Amenities</h2>
            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
            <ul class="list">
                <?php foreach ($facilities as $facility){?>
                    <li><i class="<?php echo $facility['icon'] ?>"></i> <?php echo $facility['name']?></li>
                <?php } ?>
                <!--				<li><i class="fas fa-swimming-pool"></i> Swimming pool</li>-->
                <!--				<li><i class="fas fa-dumbbell"></i> Gym & yoga</li>-->
                <!--				<li><i class="fas fa-ship"></i> Boat tours</li>-->
                <!--				<li><i class="fas fa-spa"></i> Spa & massage</li>-->
                <!--				<li><i class="fas fa-swimmer"></i> Surfing lessons</li>-->
                <!--				<li><i class="fas fa-microphone"></i> Conference room</li>-->
                <!--				<li><i class="fas fa-umbrella-beach"></i> Private Beach</li>-->
                <!--				<li><i class="fas fa-water"></i> Diving & snorkling</li>-->
            </ul>
        </div>
    </div>
</section>


<section id="rooms_hm">
    <div class="container">
        <h2>Rooms & Suites</h2>
        <p class="sm-head">Check out now our best rooms</p>

        <div class="row">
            <div class="col-md-6">
                <div class="room">
                    <div class="top lg-box">
                        <div class="img-hover-zoom"><img src="imgs/room-full-1.jpg" class="img-fluid"></div>
                        <ul class="large">
                            <li><i class="fas fa-wifi"></i></li>
                            <li><i class="fas fa-female"></i></li>
                            <li><i class="fas fa-bath"></i></li>
                            <li><i class="fas fa-utensils"></i></li>
                            <li><i class="fas fa-coffee"></i></li>
                            <li><i class="fas fa-glass-martini"></i></li>
                            <li><i class="fas fa-mobile-alt"></i></li>
                            <li><i class="fas fa-tv"></i></li>
                            <li><i class="fas fa-snowflake"></i></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3>Deluxe Room - <span>INR 3500/-</span></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="room">
                            <div class="top">
                                <ul>
                                    <li><i class="fas fa-wifi"></i></li>
                                    <li><i class="fas fa-female"></i></li>
                                    <li><i class="fas fa-bath"></i></li>
                                    <li><i class="fas fa-utensils"></i></li>
                                    <li><i class="fas fa-coffee"></i></li>
                                </ul>
                                <div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
                            </div>
                            <div class="content">
                                <h3>Honeymoon Room - <span>INR 3500/-</span></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="room">
                            <div class="top">
                                <ul>
                                    <li><i class="fas fa-wifi"></i></li>
                                    <li><i class="fas fa-female"></i></li>
                                    <li><i class="fas fa-bath"></i></li>
                                    <li><i class="fas fa-utensils"></i></li>
                                    <li><i class="fas fa-coffee"></i></li>
                                </ul>
                                <div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
                            </div>
                            <div class="content">
                                <h3>Single Room - <span>INR 3500/-</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="room">
                            <div class="top">
                                <ul>
                                    <li><i class="fas fa-wifi"></i></li>
                                    <li><i class="fas fa-female"></i></li>
                                    <li><i class="fas fa-bath"></i></li>
                                    <li><i class="fas fa-utensils"></i></li>
                                    <li><i class="fas fa-coffee"></i></li>
                                </ul>
                                <div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
                            </div>
                            <div class="content">
                                <h3>Family Room - <span>INR 3500/-</span></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="room">
                            <div class="top">
                                <ul>
                                    <li><i class="fas fa-wifi"></i></li>
                                    <li><i class="fas fa-female"></i></li>
                                    <li><i class="fas fa-bath"></i></li>
                                    <li><i class="fas fa-utensils"></i></li>
                                    <li><i class="fas fa-coffee"></i></li>
                                </ul>
                                <div class="img-hover-zoom"><img src="imgs/room-full-2.jpg" class="img-fluid"></div>
                            </div>
                            <div class="content">
                                <h3>King Room - <span>INR 3500/-</span></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php include('inc/footer.php'); ?>
<script>
    var state_id = $('#state_id option:selected').val();
    if(state_id){
        changeContentDropdown(state_id);
    }
    $('#state_id').on("change",function(){
        var state_id = $('#state_id option:selected').val();
        changeContentDropdown(state_id);
    });
    function changeContentDropdown(state) {
        $(".selectpicker#city_id").html('');
        $.ajax({
            url: "<?php echo FRONTEND_ROUTE.'get_cities.php'; ?>",
            type:"POST",
            data: {state : state},
            dataType : "json",
            success: function(data){
                if(data){
                    var options= [];
                    // $("#city_id").append("<option value=''>Select City</option>");
                    $('.selectpicker#city_id').html("<option value=''>Select City</option>");
                    $.map(data,function(val,i){

                        var city_name = val.name;
                        var option = "<option  value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>";
                        options.push(option);
                        // $("#city_id").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                    $('.selectpicker#city_id').append(options);
                    $('.selectpicker#city_id').selectpicker('refresh');
                }

            }
        });
    }
</script>
