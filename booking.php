<?php
session_start();
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

if (isset($_POST['clear'])){
    unset($_SESSION['pay']);
    unset($_SESSION['roombooking_cart']);
    message("The cart is empty.","success");
    redirect(FRONTEND_ROUTE."booking.php");

}


?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

    <section id="title">
        <h1>Book Your Stay</h1>
    </section>


<?php include('inc/search.php'); ?>

<section id="listings">
    <div class="container">
        <?php check_message();?>
        <table class="table" id="table">

            <thead>
            <tr  bgcolor="#999999">
                <!-- <th width="10">#</th> -->
                <th align="center" width="180">Room</th>
                <th align="center" width="180">Check In</th>
                <th align="center" width="180">Check Out</th>
                <th  width="120">Price</th>
                <th align="center" width="180">Nights</th>
                <th align="center" >Amount</th>
            </tr>
            </thead>
            <tbody >
            <div id="showcart"></div>

            <div id="BookingCart">
                <?php
                $mealprice = isset($_SESSION['MealPrice']) ? $_SESSION['MealPrice'] : '0';
                $payable = 0;
                if (isset( $_SESSION['roombooking_cart'])){


                    $count_cart = count($_SESSION['roombooking_cart']);

                    for ($i=0; $i < $count_cart  ; $i++) {

                        $result = $sqlQuery->SelectSingle(  "SELECT rt.title FROM `room_types` rt ,`rooms` r WHERE r.`room_type_id`=rt.`room_type_id` AND r.`room_id`='" . $_SESSION['roombooking_cart'][$i]['roomid']."'");



                            echo '<tr>';
                            // echo '<td></td>';
                            echo '<td>'. $result['title'].' </td>';
                            echo '<td>'.date_format(date_create( $_SESSION['roombooking_cart'][$i]['checkin']),"d/m/Y").'</td>';
                            echo '<td>'.date_format(date_create( $_SESSION['roombooking_cart'][$i]['checkout']),"d/m/Y").'</td>';
                            echo '<td > &pound; '. $_SESSION['roombooking_cart'][$i]['roomprice'].'
                          <input type="hidden" value="'.$_SESSION['roombooking_cart'][$i]['roomprice'].'"  name="roomprice'.$_SESSION['roombooking_cart'][$i]['roomid'].'" id="roomprice'.$_SESSION['roombooking_cart'][$i]['roomid'].'"/>

                        </td>';
                            echo '<td><input style="border:0px" readonly type="number" value="'.$_SESSION['roombooking_cart'][$i]['day'].'" id="day'.$_SESSION['roombooking_cart'][$i]['roomid'].'" name="day'.$_SESSION['roombooking_cart'][$i]['roomid'].'" /></td>';

                            echo '</td>';
                            echo '<td>&pound; <output id="TotAmount'.$_SESSION['roombooking_cart'][$i]['roomid'].'" >'.$_SESSION['roombooking_cart'][$i]['roomprice'].'</output></td>';


                        $payable += $_SESSION['roombooking_cart'][$i]['roomprice'] ;





                    }

                    $_SESSION['pay'] = $payable;

                }
                ?>
            </div>
            </tbody>

            <tfoot>
            <tr>
                <td colspan="5"><h4 align="right">Total:</h4></td>
                <td colspan="4">
                    <h4><b>&pound;<span id="sum"><?php  echo isset($_SESSION['pay']) ?  $_SESSION['pay'] :'Your booking cart is empty.';?></span></b></h4>

                </td>
            </tr>
            </tfoot>
        </table>

        <form method="post" action="">
            <div class="row" >
                <?php
                if (isset($_SESSION['roombooking_cart'])){
                    ?>
                    <button type="submit" class="btn btn-secondary " name="clear">Clear Cart</button>
                    <?php

                    if (isset($_SESSION['user_session']) && $_SESSION['user_session']['role_id'] == TRAVEL_AGENT_ROLE_ID){
                        ?>
                        <div  class="btn btn-info " ><a class="text-decoration-none text-white" href="<?php echo FRONTEND_ROUTE; ?>customer_details.php" name="continue">Continue Booking</a></div>
                        <?php
                    }elseif(isset($_SESSION['user_session']) && $_SESSION['user_session']['role_id'] == CUSTOMER_ROLE_ID){ ?>
                        <div  class="btn btn-info " ><a class="text-decoration-none text-white" href="<?php echo FRONTEND_ROUTE; ?>booking_details.php"  name="continue">Continue Booking</a></div>
                        <?php
                    }else{ ?>
                        <div  class="btn btn-info " ><a class="text-decoration-none text-white" href="<?php echo FRONTEND_ROUTE; ?>login_register.php"  name="continue">Continue Booking</a></div>
                        <?php
                    }
                }

                ?>


            </div>

        </form>

    </div>
</section>



<?php include('inc/footer.php'); ?>
