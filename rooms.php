<?php

session_start();
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
if (!isset($_SESSION['bookingSearchDetails']['checkin']) &&!isset($_SESSION['bookingSearchDetails']['checkout'])) {
    message('Please select checkin,checkout dates and check availability to proceed further','info');
    redirect(FRONTEND_ROUTE . 'hotels.php');
}
if (isset($_GET['hotel_id']) && !empty($_GET['hotel_id'])) {
    $hotel_id = $_GET['hotel_id'];
    $_SESSION['bookingSearchDetails']['hotel_id'] = $hotel_id ;

    $rooms = $sqlQuery->SelectQuery('SELECT rooms.*,room_types.title,room_types.description FROM rooms INNER JOIN room_types ON room_types.room_type_id = rooms.room_type_id  WHERE hotel_id ="' . $hotel_id . '" and room_id NOT IN(SELECT room_id FROM bookings where "'.date('Y-m-d',strtotime($_SESSION['bookingSearchDetails']['checkin'])).'" BETWEEN check_in and check_out and hotel_id = "'.$hotel_id.'")');

//    $rooms = $sqlQuery->SelectQuery('SELECT rooms.*,room_types.title,room_types.description FROM rooms INNER JOIN room_types ON room_types.room_type_id = rooms.room_type_id  WHERE hotel_id ="' . $hotel_id . '" ');
} else {
    redirect(FRONTEND_ROUTE . 'index.php');
}

foreach ($rooms as $key => $record) {
    $rooms[$key]['room_facilities'] = $sqlQuery->SelectQuery('SELECT room_facilities.room_id,facilities.name,facilities.icon FROM room_facilities INNER JOIN facilities ON facilities.facility_id = room_facilities.facility_id  WHERE room_id ="' . $record['room_id'] . '" ');
}
if (isset($_POST['bookRoom'])) {
    $numberOfNights = 0;
    if (isset($_SESSION['bookingSearchDetails']['checkin']) && $_SESSION['bookingSearchDetails']['checkout']) {

        $date1 = new DateTime($_SESSION['bookingSearchDetails']['checkin']);
        $date2 = new DateTime($_SESSION['bookingSearchDetails']['checkout']);
        $numberOfNights = $date2->diff($date1)->format("%a");
    }
    if ($numberOfNights <= 0) {
        $totalprice = $_POST['room_price'] * 1;
        $days = 1;
    } else {
        $totalprice = $_POST['room_price'] * $numberOfNights;
        $days = $numberOfNights;
    }

    addtocart($hotel_id, $_POST['room_id'], $days, $totalprice, date('Y-m-d', strtotime($_SESSION['bookingSearchDetails']['checkin'])), date('Y-m-d', strtotime($_SESSION['bookingSearchDetails']['checkout'])));
//unset($_SESSION['bookingSearchDetails']);

    redirect(FRONTEND_ROUTE . 'booking.php');

}

?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
    <h1>Room Listings</h1>
</section>


<?php include('inc/search.php'); ?>


<section id="listings">
    <div class="container">

        <?php if (isset($rooms) && count($rooms) > 0) {
            foreach ($rooms as $economy_rooms) {
                ?>
                <div class="inner_box">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="img-hover-zoom"><img
                                        src="<?php echo FRONTEND_UPLOAD_PATH_ORG . 'room_images/' . $economy_rooms['image'] ?>"
                                        class="img-fluid"></div>
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-9">
                                        <h2><?php echo $economy_rooms['title'] ?></h2>
                                        <p><?php echo $economy_rooms['description'] ?></p>

<!--                                        <ul class="large">-->
<!--                                            --><?php
//                                            if (isset($economy_rooms['room_facilities']) && count($economy_rooms['room_facilities']) > 0) {
//                                                foreach ($economy_rooms['room_facilities'] as $roomFacility) { ?>
<!--                                                    <li>-->
<!--                                                        <i class="--><?php //echo $roomFacility['icon'] ?><!--"></i> --><?php //echo $roomFacility['name'] ?>
<!--                                                    </li>-->
<!--                                                --><?php //}
//                                            } ?>
<!--                                        </ul>-->
                                    </div>
                                    <div class="col-md-3">
                                        <div class="price_box">
                                            <h3>
                                                <i class="fas fa-pound-sign"></i> <?php echo $economy_rooms['room_charges'] ?>
                                            </h3>
                                            <p class="sm-head">per night</p>
                                            <a class="btn btn-details" href="room-details.php?room_id=<?php echo $economy_rooms['room_id'] ?>"> More Details</a>
<!--                                            <form method="post">-->
<!--                                                <input type="hidden" name="room_id"-->
<!--                                                       value="--><?php //echo $economy_rooms['room_id'] ?><!--">-->
<!--                                                <input type="hidden" name="room_price"-->
<!--                                                       value="--><?php //echo $economy_rooms['room_charges'] ?><!--">-->
<!--                                                <button class="btn btn-details" name="bookRoom">Book Now</button>-->
<!--                                            </form>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        } ?>
    </div>
</section>



<?php include('inc/footer.php'); ?>

