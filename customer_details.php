<?php
session_start();
unset($_SESSION['register_user_details']);
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();

if(!isset($_SESSION['user_session']) && $_SESSION['user_session']['roli_id'] == TRAVEL_AGENT_ROLE_ID){
    redirect('index.php');
}
if (!isset($_SESSION['roombooking_cart'])) {
    redirect('index.php');
}

    if (isset($_POST['register_details_submit'])) {

//        $filepath = UPLOAD_PATH_ORG . 'userid_proofs/';
//        $filename = $custUserId . '-' . $_FILES['id_proof']['name'];

//        $customerId = $sqlQuery->InsertQuery('INSERT INTO customers SET  first_name = "' . $_POST['fname'] . '", last_name = "' . $_POST['lname'] . '", street_address = "' . $_POST['address'] . '", city_id = "' . $_POST['user_city_id'] . '", state_id = "' . $_POST['user_state_id'] . '", zip = "' . $_POST['zipcode'] . '", contact_no = "' . $_POST['contact_no'] . '", email_id = "' . $_POST['emailid'] . '", id_proof ="' . $filename . '"');

        $customerId = $sqlQuery->InsertQuery('INSERT INTO customers SET  first_name = "' . $_POST['fname'] . '", last_name = "' . $_POST['lname'] . '", street_address = "' . $_POST['address'] . '", city_id = "' . $_POST['user_city_id'] . '", state_id = "' . $_POST['user_state_id'] . '", zip = "' . $_POST['zipcode'] . '", contact_no = "' . $_POST['contact_no'] . '", email_id = "' . $_POST['emailid'] . '"');


            $_SESSION['user_session']['new_customer_id'] = $customerId;

//        if (!file_exists($filepath)) {
//            mkdir($filepath, 0777, true);
//        }
//        $target_file = $filepath . $filename;
//
//        if (move_uploaded_file($_FILES['images']['tmp_name'][0], $target_file)) {
//            echo "The file " . htmlspecialchars(basename($_FILES['images']['name'][0])) . " has been uploaded.";
//        } else {
//            echo "Sorry, there was an error uploading your file.";
//        }
//
//        $customerData = $sqlQuery->SelectSingle("Select * from customers where customer_id=".$customerId);
//        echo"<script>alert('Account created successfully. Please login to your account to proceed further.')</script>";
        redirect('booking_details.php');
}
?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
    <h1>Customer Details</h1>
</section>
<style>
    .uploaded {
        width: 100%;
    }

    .uploaded .uploaded-image {
        display: inline-block;
        width: 33%;
        padding: 5px;
    }

    .uploaded img {
        height: 100px;
        width: 100%;
    }

    .btn-light {
        color: #212529;
        background-color: #ffffff;
        border-color: #c6c6c6;
    }
</style>

<?php include('inc/search.php'); ?>
<section id="listings">
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation"><a class="nav-link active" id="register-tab" data-toggle="tab"
                                                        href="#booking_register" role="tab"
                                                        aria-controls="booking_register"
                                                        aria-selected="false">Customer Details</a></li>

        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="booking_register" role="tabpanel" aria-labelledby="register-tab">
                <div class="log-regis-form">
                    <h3>Register</h3>
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" name="fname" class="form-control" placeholder="First Name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="emailid" class="form-control" placeholder="Email ID" required>
                        </div>

                        <div class="form-group">
                            <input type="text" name="contact_no" class="form-control" placeholder="Contact No" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control selectpicker" data-live-search="true" name="user_state_id"
                                    id="user_state_id" required>
                                <option value="">Select County</option>
                                <?php foreach ($county as $data) { ?>
                                    <option value="<?php echo $data['id'] ?>" <?php echo (isset($_POST['state_id']) && !empty($_POST['state_id']) && $_POST['state_id'] == $data['id']) ? 'selected' : '' ?>><?php echo $data['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control selectpicker" data-live-search="true" name="user_city_id"
                                    id="user_city_id" required>
                                <option value="">Select City</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="zipcode" class="form-control" placeholder="Zipcode" required>
                        </div>
                        <div class="form-group">
                            <textarea name="address" class="form-control" placeholder="Address" required></textarea>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label for="uploadimage">Id Proof</label>-->
<!--                            <input type="file" name="id_proof" required>-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <input type="text" name="username" class="form-control" placeholder="Username" required>-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <input type="password" name="password" class="form-control" placeholder="Password" required>-->
<!--                        </div>-->
                        <div class="form-group">
                            <button type="submit" name="register_details_submit" class="btn btn-login">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

<?php include('inc/footer.php'); ?>
<script>
    var state_id = $('#user_state_id option:selected').val();
    if (state_id) {
        stateCityDropdown(state_id);
    }
    $('#user_state_id').on("change", function () {
        var state_id = $('#user_state_id option:selected').val();
        stateCityDropdown(state_id);
    });

    function stateCityDropdown(state) {
        $(".selectpicker#user_city_id").html('');
        $.ajax({
            url: "<?php echo FRONTEND_ROUTE . 'get_cities.php'; ?>",
            type: "POST",
            data: {state: state},
            dataType: "json",
            success: function (data) {
                if (data) {
                    var selectedCityId = "<?php echo (isset($_POST['city_id']) && !empty($_POST['city_id'])) ? $_POST['city_id'] : '' ?>";
                    var options = [];
                    // $("#user_city_id").append("<option value=''>Select City</option>");
                    $('.selectpicker#user_city_id').html("<option value=''>Select City</option>");
                    $.map(data, function (val, i) {

                        var city_name = val.name;
                        var selected = (selectedCityId == val.id) ? "selected=selected" : "";
                        var option = "<option  " + selected + "   value='" + val.id + "' data-id='" + val.id + "'>" + city_name + "</option>";
                        options.push(option);
                        // $("#user_city_id").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                    $('.selectpicker#user_city_id').append(options);
                    $('.selectpicker#user_city_id').selectpicker('refresh');
                }

            }
        });
    }

    var state_id = $('#agent_state_id option:selected').val();
    if (state_id) {
        agentStateCityDropdown(state_id);
    }
    $('#agent_state_id').on("change", function () {
        var state_id = $('#agent_state_id option:selected').val();
        agentStateCityDropdown(state_id);
    });

    function agentStateCityDropdown(state) {
        $(".selectpicker#agent_city_id").html('');
        $.ajax({
            url: "<?php echo FRONTEND_ROUTE . 'get_cities.php'; ?>",
            type: "POST",
            data: {state: state},
            dataType: "json",
            success: function (data) {
                if (data) {
                    var selectedCityId = "<?php echo (isset($_POST['city_id']) && !empty($_POST['city_id'])) ? $_POST['city_id'] : '' ?>";
                    var options = [];
                    // $("#agent_city_id").append("<option value=''>Select City</option>");
                    $('.selectpicker#agent_city_id').html("<option value=''>Select City</option>");
                    $.map(data, function (val, i) {

                        var city_name = val.name;
                        var selected = (selectedCityId == val.id) ? "selected=selected" : "";
                        var option = "<option  " + selected + "   value='" + val.id + "' data-id='" + val.id + "'>" + city_name + "</option>";
                        options.push(option);
                        // $("#agent_city_id").append("<option "+selected+" value='" +  val.id + "' data-id='"+ val.id +"'>"+ city_name +"</option>");
                    });
                    $('.selectpicker#agent_city_id').append(options);
                    $('.selectpicker#agent_city_id').selectpicker('refresh');
                }

            }
        });
    }
</script>
