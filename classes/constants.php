<?php
define("BASE_PATH", "/assignment_online_hotel_booking");
define("APPLICATION_PATH", $_SERVER['DOCUMENT_ROOT'] . BASE_PATH);
define("UPLOAD_PATH_ORG", APPLICATION_PATH . "/uploads/");
define("FRONTEND_UPLOAD_PATH_ORG", BASE_PATH . "/uploads/");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "assignment_online_hotelbookfinal");
//include($_SERVER['DOCUMENT_ROOT'].ROOT_PATH.'/classes/SqlQueries.php');
$facilityType = array('1'=>'Hotel','2' => 'Room');
$hotelType = array('3'=>'3 star','4' => '4 star','5' => '5 star');
define('ADMIN_ROUTE', BASE_PATH.'/admin/');
define('FRONTEND_ROUTE', BASE_PATH.'/');
define('ADMIN_DOCUMENT_ROUTE', $_SERVER['DOCUMENT_ROOT'].BASE_PATH.'/admin/');

define('HOTEL_IMAGE_UPLOAD_PATH',UPLOAD_PATH_ORG.'hotel_images/');
define('ROOM_IMAGE_UPLOAD_PATH',UPLOAD_PATH_ORG.'room_images/');
define('IMAGE_UPLOAD_PATH',$_SERVER['DOCUMENT_ROOT'] .'/assignment_online_hotel_booking/admin/');
define('ADMIN_ROLE_ID',1);
define('CUSTOMER_ROLE_ID',2);
define('STAFF_ROLE_ID',3);
define('TRAVEL_AGENT_ROLE_ID',4);

