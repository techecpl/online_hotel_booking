<?php
function addtocart($hotelid,$pid,$day, $price,$checkin,$checkout,$guest,$kids){
    if($pid<1 or $day<1) return;
    if (!empty($_SESSION['roombooking_cart'])){


        if(is_array($_SESSION['roombooking_cart'])){
            if(product_exists($pid)) return;
            $max=count($_SESSION['roombooking_cart']);
            $_SESSION['roombooking_cart'][$max]['hotelid']=$hotelid;
            $_SESSION['roombooking_cart'][$max]['roomid']=$pid;
            $_SESSION['roombooking_cart'][$max]['day']=$day;
            $_SESSION['roombooking_cart'][$max]['roomprice']=$price;
            $_SESSION['roombooking_cart'][$max]['checkin']=$checkin;
            $_SESSION['roombooking_cart'][$max]['checkout']=$checkout;
            $_SESSION['roombooking_cart'][$max]['guest'] = $guest;
            $_SESSION['roombooking_cart'][$max]['kids'] = $kids;

        }
        else{
            $_SESSION['roombooking_cart']=array();
            $_SESSION['roombooking_cart'][0]['hotelid']=$hotelid;
            $_SESSION['roombooking_cart'][0]['roomid']=$pid;
            $_SESSION['roombooking_cart'][0]['day']=$day;
            $_SESSION['roombooking_cart'][0]['roomprice']=$price;
            $_SESSION['roombooking_cart'][0]['checkin']=$checkin;
            $_SESSION['roombooking_cart'][0]['checkout']=$checkout;
            $_SESSION['roombooking_cart'][0]['guest'] = $guest;
            $_SESSION['roombooking_cart'][0]['kids'] = $kids;

        }
    }else{
        $_SESSION['roombooking_cart']=array();
        $_SESSION['roombooking_cart'][0]['hotelid']=$hotelid;
        $_SESSION['roombooking_cart'][0]['roomid']=$pid;
        $_SESSION['roombooking_cart'][0]['day']=$day;
        $_SESSION['roombooking_cart'][0]['roomprice']=$price;
        $_SESSION['roombooking_cart'][0]['checkin']=$checkin;
        $_SESSION['roombooking_cart'][0]['checkout']=$checkout;
        $_SESSION['roombooking_cart'][0]['guest'] = $guest;
        $_SESSION['roombooking_cart'][0]['kids'] = $kids;


    }
}

function redirect_to($location = NULL) {
    if($location != NULL){
        header("Location: {$location}");
        exit;
    }
}
function redirect($location=Null){
    if($location!=Null){
        echo "<script>
					window.location='{$location}'
				</script>";
    }else{
        echo 'error location';
    }

}

function product_exists($pid){
    $pid=intval($pid);
    $max=count($_SESSION['roombooking_cart']);
    $flag=0;
    for($i=0;$i<$max;$i++){
        if($pid==$_SESSION['roombooking_cart'][$i]['roomid']){
            $flag=1;

            message("Item is already in the cart.","success");
            break;
        }
    }
    return $flag;
}

function message($msg="", $msgtype="") {
    if(!empty($msg)) {
        // then this is "set message"
        // make sure you understand why $this->message=$msg wouldn't work
        $_SESSION['message'] = $msg;
        $_SESSION['msgtype'] = $msgtype;
    } else {
        // then this is "get message"
        return $message;
    }
}

function check_message(){
    if(isset($_SESSION['message'])){
        if(isset($_SESSION['msgtype'])){
            if ($_SESSION['msgtype']=="info"){
                echo  '<div class="alert alert-info text-center">'. $_SESSION['message'] . '</div>';

            }elseif($_SESSION['msgtype']=="error"){
                echo  '<div class="alert alert-danger text-center">' . $_SESSION['message'] . '</div>';

            }elseif($_SESSION['msgtype']=="success"){
                echo  '<div class="alert alert-success text-center">' . $_SESSION['message'] . '</div>';
            }
            unset($_SESSION['message']);
            unset($_SESSION['msgtype']);
        }

    }
    return false;
}
function removetocart($pid){
    $pid=intval($pid);
    $max=count($_SESSION['dragonhouse_cart']);
    for($i=0;$i<$max;$i++){
        if($pid==$_SESSION['dragonhouse_cart'][$i]['dragonhouseroomid']){
            unset($_SESSION['dragonhouse_cart'][$i]);
            break;
        }
    }
    $_SESSION['dragonhouse_cart']=array_values($_SESSION['dragonhouse_cart']);
}