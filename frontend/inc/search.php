<?php $county = $sqlQuery->SelectQuery("SELECT * FROM states ORDER BY name ASC");?>
<section id="search_bar_top">
	<div class="container">
		<form class="form-inline" role="form" method="post" action="<?php echo 'http://'.$_SERVER['HTTP_HOST'].FRONTEND_ROUTE.'hotels.php'?>">
            <div class="form-group">
                <select class="form-control selectpicker" data-live-search="true" name="state_id" id="state_id">
                    <option value="">Select County</option>
                    <?php foreach ($county as $data){ ?>
                        <option value="<?php echo $data['id']?>" <?php echo (isset($_SESSION['bookingSearchDetails']['state_id']) && !empty($_SESSION['bookingSearchDetails']['state_id']) && $_SESSION['bookingSearchDetails']['state_id'] == $data['id']) ? 'selected' :'' ?>><?php echo $data['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control selectpicker" data-live-search="true" name="city_id" id="city_id">
                    <option value="">Select City</option>

                </select>
            </div>
			<div class="form-group">
				<input type="text" name="checkin" class="form-control" placeholder="Check-in" id="datepicker" value="<?php echo (isset($_SESSION['bookingSearchDetails']['checkin']) && !empty($_SESSION['bookingSearchDetails']['checkin'])) ? $_SESSION['bookingSearchDetails']['checkin'] :'' ?>">
			</div>
			<div class="form-group">
				<input type="text" name="checkout" class="form-control" placeholder="Check-out" id="datepicker2" value="<?php echo (isset($_SESSION['bookingSearchDetails']['checkout']) && !empty($_SESSION['bookingSearchDetails']['checkout'])) ? $_SESSION['bookingSearchDetails']['checkout'] :'' ?>">
			</div>
			<div class="form-group">				
				<select name="guest" class="form-control">
					<option value="">---Guest--</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>				
			</div>
			<div class="form-group">
				<select name="kids" class="form-control">
					<option value="">---Kids--</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>				
			</div>
			<div class="form-group">
				<button type="submit" name="submitHotelSearchDetails" class="btn btn-book">Check Avaiability</button>
			</div>
		</form>

	</div>
</section>