<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
	<div class="container">
    	<a class="navbar-brand" href="index.php"><img src="imgs/logo.png"></a>
    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          	<i class="fas fa-bars"></i>
        </button>
    	<div class="collapse navbar-collapse" id="navbarResponsive">
      		<ul class="navbar-nav ml-auto">
        		<li class="nav-item active"><a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a></li>
        		<li class="nav-item"><a class="nav-link" href="hotels.php">Hotels</a></li>

        		
        		<?php
                if(isset($_SESSION['user_session'])){
                    echo'<li class="nav-item"><a class="nav-link" href="logout.php">Hello, '.$_SESSION['user_session']['username'].'</a><a href="logout.php"><i class="fas fa-sign-out-alt"></i></a></li>';
                }else{
                    echo'<li class="nav-item"><a class="nav-link" href="login_register.php">Login/Register</a></li>';
                }
                ?>


      		</ul>
    	</div>
  	</div>
</nav>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login/Register</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Login</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">Register</a></li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                        <div class="log-regis-form">
                            <h3>Login</h3>
                            <form>
                                <div class="form-group">
                                    <input type="email" name="username" class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-login">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                        <div class="log-regis-form">
                            <h3>Register</h3>
                            <form>
                                <div class="form-group">
                                    <input type="text" name="fname" class="form-control" placeholder="First Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="lname" class="form-control" placeholder="Last Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="emailid" class="form-control" placeholder="Email ID">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-login">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>