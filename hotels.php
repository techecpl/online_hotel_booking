<?php
session_start();
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
$where = "";
if (isset($_POST['submitHotelSearchDetails'])) {
    $_SESSION['bookingSearchDetails'] = $_POST;
    if (isset($_POST['state_id']) && !empty($_POST['state_id']) && isset($_POST['city_id']) && !empty($_POST['city_id'])) {
        $where .= " WHERE hotels.state_id ='" . $_POST['state_id'] . "' and hotels.city_id ='" . $_POST['city_id'] . "'";
    } elseif (isset($_POST['state_id']) && !empty($_POST['state_id'])) {
        $where .= " WHERE hotels.state_id ='" . $_POST['state_id'] . "'";
    }
}
$hotels = $sqlQuery->SelectQuery("SELECT hotels.*,states.name as state_name, cities.name as city_name FROM hotels INNER JOIN states ON states.id = hotels.state_id INNER JOIN cities ON cities.id = hotels.city_id {$where} ");
foreach ($hotels as $key => $record) {
    $hotels[$key]['hotel_facilities'] = $sqlQuery->SelectQuery('SELECT hotel_facilities.*,facilities.name,facilities.icon FROM hotel_facilities INNER JOIN facilities ON facilities.facility_id = hotel_facilities.facility_id  WHERE hotel_id ="' . $record['hotel_id'] . '" ');
}
?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
    <h1>Hotel Listings</h1>
</section>


<?php include('inc/search.php'); ?>

<?php check_message();?>
<section id="listings">
    <div class="container">
        <?php if (isset($hotels) && count($hotels) > 0) {
            foreach ($hotels as $hotel) {
                ?>
                <div class="inner_box">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="img-hover-zoom"><img
                                        src="<?php echo FRONTEND_UPLOAD_PATH_ORG . 'hotel_images/' . $hotel['image'] ?>"
                                        class="img-fluid"></div>
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2><?php echo $hotel['name'] ?></h2>
                                        <p><?php echo !empty($hotel['description']) ? $hotel['description'] : '' ?></p>
                                        <div class="star-rating">
                                            <ul class="list-inline">
                                                <?php
                                                for ($i = 1; $i <= $hotel['type']; $i++) {
                                                    ?>
                                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <ul class="large">
                                            <?php foreach ($hotel['hotel_facilities'] as $h_facilities) { ?>
                                                <li title="<?php echo $h_facilities['name']?>"><i class="<?php echo $h_facilities['icon'] ?>"></i></li>

                                            <?php } ?>
                                        </ul>
                                        <div class="price_box">
                                            <a href="rooms.php?hotel_id=<?php echo $hotel['hotel_id']; ?>"
                                               class="btn btn-details">Book Rooms</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        } else { ?>
            <section id="not_found">
                <div class="container">
                    <h3>Sorry! No Hotel Found</h3>
                    <img src="imgs/notfound.jpg" alt="" class="img-fluid">
                </div>
            </section>
        <?php } ?>
    </div>
</section>


<?php include('inc/footer.php'); ?>

