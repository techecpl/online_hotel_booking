<?php
session_start();
include("classes/SqlQueries.php");
$sqlQuery = new SqlQueries();
if(!isset($_SESSION['user_session'] )){
    redirect(FRONTEND_ROUTE . 'index.php');
}
if (!isset($_SESSION['roombooking_cart'])) {
    redirect(FRONTEND_ROUTE . 'index.php');
}
$roomDetails = array();
$roomDetails = $_SESSION['roombooking_cart'];
foreach ($_SESSION['roombooking_cart'] as $key => $bookDetails) {
    $roomTitle = $sqlQuery->SelectSingle("SELECT rt.title FROM `room_types` rt ,`rooms` r WHERE r.`room_type_id`=rt.`room_type_id` AND r.`room_id`='" . $bookDetails['roomid'] . "'");
    $roomDetails[$key]['title'] = $roomTitle['title'];
}


if (isset($_POST['booking_details_submit'])) {

    foreach ($_SESSION['roombooking_cart'] as $key => $value) {
        $_SESSION['roombooking_cart'][$key]['room_type'] = $_POST['room_type'][$key];
        $_SESSION['roombooking_cart'][$key]['guest'] = $_POST['guest'][$key];
        $_SESSION['roombooking_cart'][$key]['kids'] = $_POST['kids'][$key];
    }

    redirect('payment.php');
}
?>

<?php include('inc/header.php'); ?>

<?php include('inc/navbar.php'); ?>

<section id="title">
    <h1>Book Your Stay</h1>
</section>
<style>
    .uploaded {
        width: 100%;
    }

    .uploaded .uploaded-image {
        display: inline-block;
        width: 33%;
        padding: 5px;
    }

    .uploaded img {
        height: 100px;
        width: 100%;
    }

    .btn-light {
        color: #212529;
        background-color: #ffffff;
        border-color: #c6c6c6;
    }
</style>

<?php include('inc/search.php'); ?>
<section id="listings">
    <div class="container">

        <h3>Room Details</h3>
        <form method="post" enctype="multipart/form-data">

            <?php foreach ($roomDetails as $key => $roomDetail) { ?>
                <h6><?php echo 'Room:- ' . ++$key ?></h6>
                <div class="form-group">
                    <input type="text" name="room_type[]" class="form-control"
                           value="<?php echo $roomDetail['title']; ?>" required>
                </div>
                <div class="form-group">
                    <input type="text" name="checkin[]" class="form-control datepicker" placeholder="Check-in"
                           value="<?php echo (isset($roomDetail['checkin']) && !empty($roomDetail['checkin'])) ? $roomDetail['checkin'] : '' ?>"
                           required>
                </div>
                <div class="form-group">
                    <input type="text" name="checkout[]" class="form-control datepicker" placeholder="Check-out"
                           value="<?php echo (isset($roomDetail['checkout']) && !empty($roomDetail['checkout'])) ? $roomDetail['checkout'] : '' ?>"
                           required>
                </div>
                <div class="form-group">
                    <select name="guest[]" class="form-control">
                        <option value="">---Guest--</option>
                        <?php
                        for($i=1;$i<=5;$i++){
                            ?>
                            <option value="<?php echo $i; ?>" <?php echo (isset($roomDetail['guest']) && !empty($roomDetail['guest']))?(($roomDetail['guest']==$i)?'selected':''):'' ?>><?php echo $i; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <select name="kids[]" class="form-control">
                        <option value="">---Kids--</option>
                        <?php
                        for($i=1;$i<=5;$i++){
                            ?>
                            <option value="<?php echo $i; ?>" <?php echo (isset($roomDetail['kids']) && !empty($roomDetail['kids']))?(($roomDetail['kids']==$i)?'selected':''):'' ?>><?php echo $i; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            <?php } ?>

            <div class="form-group">
                <button type="submit" name="booking_details_submit" class="btn btn-login">Book Now</button>
            </div>
        </form>


    </div>
</section>

<?php include('inc/footer.php'); ?>

